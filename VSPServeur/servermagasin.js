const express = require('express');
const bodyParser = require('body-parser');
var cons = require('consolidate');

//const request = require('request');
//const http = require('https');

const path = require('path');
const cookieParser = require("cookie-parser");
const JwtGenerator = require("./scripts/jwt-generator");

const compression = require("compression");
const helmet = require("helmet");

var cookies = require('cookies')

const request = require("request-promise");
const moment = require("moment");



const app = express();

app.use(bodyParser.json());
app.use(cookieParser());
app.use(compression());


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.engine('html', cons.swig)
app.set("view engine", "html");
app.set("views", path.join(__dirname, "./dist"));

app.use(express.static(path.join(__dirname, './dist')));

const port = 3001;

//Access Token Management #####################################################################


async function generateAccessToken() {
  const jwtGenerator = new JwtGenerator();
  const token = jwtGenerator.generateToken("");
  const authRequest = {
      method: "POST",
      uri: "https://login.salesforce.com/services/oauth2/token",
      form: {
          "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
          "assertion": token
      }
  }
  try {
      const sfResponse = JSON.parse(await request(authRequest));
      console.log(`sfResponse ${JSON.stringify(sfResponse)}`);
      return sfResponse.access_token;

  } catch(err){
      console.log(err);
	  throw err;
  }
}


// WSO2 Call For HMAC Key Verification ########################################################

async function wso2HMACValidation(req) {

	var wso2Token = await wso2Auth();
	
	console.log('wso2Token : ' + wso2Token);
	
	const validationRequest = {
		url: 'https://am-prod.verspieren.com/dpas_ed_vlocity/1.0.1/ed_subscribe',
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : 'Bearer ' + wso2Token
		},
		json: {

			donnees_client : req.body.donnees_client,
			emetteur : req.body.emetteur ,
			fonction : req.body.fonction,
			horodatage_flux : req.body.horodatage_flux,
			identifiant_transaction_magasin : req.body.identifiant_transaction_magasin,
			MAC : req.body.MAC
		},
		qs: {
			donnees_client : req.body.donnees_client,
			emetteur : req.body.emetteur ,
			fonction : req.body.fonction,
			horodatage_flux : req.body.horodatage_flux,
			identifiant_transaction_magasin : req.body.identifiant_transaction_magasin,
			MAC : req.body.MAC
		}
	};

	try {
      const validationResponse = await request(validationRequest);
      console.log(`validationResponse ${JSON.stringify(validationResponse)}`);
      return validationResponse.status;

	} catch(err){
      console.log(err);
	  throw err;
	}

}

// WSO2 Authentication Call

async function wso2Auth() {

	const requestOptions = {
		url: 'https://am-prod.verspieren.com/token',
		method: 'POST',
		headers: {
			"Content-Type": 'application/x-www-form-urlencoded',
			"Authorization" : 'Basic NkxkOHNBNmUySWlwZnlSeFFoNTY2Q093Z0o4YTo1ejFYcjd4cldzbF9BbWlnQ1d6RVdUcndtemth'
		},
		json: {},
		qs: {
			grant_type : 'client_credentials'
		}
	};
	
	try {
      const wso2AuthResponse = await request(requestOptions);
      console.log(`wso2AuthResponse ${JSON.stringify(wso2AuthResponse)}`);
      return wso2AuthResponse.access_token;

	} catch(err){
      console.log(err);
	  throw err;
	}

}

function b64_to_utf8( str ) {
		return decodeURIComponent(escape(window.atob( str )));
	  }

// API POST ####################################################################################

//electro-cofidis.verspieren.com = Magasin

//electrosur.verspieren.com = Web

app.post('/', async (req, res) => {
		
	//Decode Base64 ##############################################################
	
	let data = req.body.donnees_client;
	
	console.log('data : ' + data);
	

	//Call WSO2 ##################################################################
	
	
	try{
		var validationHmac = await wso2HMACValidation(req);
		
		console.log('validationHmac : ' + validationHmac);
		
		if(validationHmac == 'OK'){
			
			//#Reucprétaion de l'access_token
			var accesstoken = await generateAccessToken();
			
			console.log('accesstoken : ' + accesstoken);
			
			res.render('magasin', { "CustomerData": data , "Token": accesstoken});
			
		}
		
		else{
			
			res.render('magasinErr', {"errorMessage" : "Clé HMAC Cofidis non valide"});
		}
	
	}catch(error){
		
		res.render('magasinErr', {"errorMessage" : error.toString()});
	}		
	
	
});

//electrosur.verspiere.com


app.get("/resource/1617887509000/Electrosur_Notice_information", function (req, res) {
    res.sendfile('dist/Electrosur_Notice_information.pdf');
});

app.get("/resource/1617887577000/Electrosur_IPID", function (req, res) {
    res.sendfile('dist/Electrosur_IPID.pdf');
});


app.listen(port, () => {
    console.log(`Express server listening on PORT: ${port}!`);
});