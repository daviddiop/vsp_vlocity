// Custom webpack configuration file, provides generation of service worker
// More information: https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin
const cookieParser = require("cookie-parser");
const compression = require("compression");
const bodyParser = require('body-parser');
const request = require("request-promise");
const moment = require("moment");
const JwtGenerator = require("./jwt-generator");
const express = require("express"),
path = require("path"),
webpack = require("webpack"),
defaultBuilder = require("@vlocity-ins/resources/src/javascript/utils/webpackBuilder"),
package = require("../package.json");

const componentNamespace = package.componentNamespace || "cmp";
const omniscriptNamespace = package.omniscriptNamespace || "vlocityomniscript";

// Import required package configurations
const resourcesConfig = defaultBuilder(
    path.resolve(
      __dirname,
      `../node_modules/@vlocity-ins/resources/src/javascript/salesforce/`
    )
  ),
  coreConfig = defaultBuilder(
    path.resolve(
      __dirname,
      `../node_modules/@vlocity-ins/core/src/javascript/salesforce/`
    )
  ),
  componentsConfig = defaultBuilder(
    path.resolve(
      __dirname,
      `../node_modules/@vlocity-ins/components/src/javascript/salesforce/`
    )
  ),
  ouiConfig = defaultBuilder(
    path.resolve(
      __dirname,
      `../node_modules/@vlocity-ins/oui/src/javascript/salesforce/`
    )
  );

// Create the alias for package specify needs
const alias = {
  ...resourcesConfig.resolve.alias,
  ...coreConfig.resolve.alias,
  ...componentsConfig.resolve.alias,
  ...ouiConfig.resolve.alias,
  "vlocityoverride/redirects": path.resolve(
    __dirname,
    "../src/modules/vlocityoverride/redirects.js"
  ),
  "vlocitytranslations/translations": path.resolve(
    __dirname,
    "../src/modules/vlocitytranslations/translations.js"
  ),
};

// Export webpack config
module.exports = {
  output: {
    publicPath: "/",
  },
  plugins: [
    new webpack.DefinePlugin({
      COMPONENT_NAMESPACE: JSON.stringify(componentNamespace),
      OMNISCRIPT_NAMESPACE: JSON.stringify(omniscriptNamespace),
      SF_ORG_NAMESPACE: JSON.stringify(process.env.SF_ORG_NAMESPACE),
      // Set to true enable navigation using the c-router, or c-omniscript-router,
      // without triggering a page reload.
      VLOCITY_SPA_NAVIGATION: false,
    }),
  ],
  devServer: {
    before: function (app, server, compiler) {

      app.use(cookieParser());
      app.use(compression());
      app.use(bodyParser.json());

      app.use(async (req, res, next) => {
          const cookie = req.cookies.oauthCookie;
          if (cookie === undefined){
              const token = await generateAccessToken();
              let currentTime = moment().format();
              let expirationTime = moment(currentTime).add(1, 'hours');
              res.cookie('oauthCookie', `${token};Expiration=${expirationTime}`, { maxAge: 10800000, httpOnly: false });
              console.log(`created new cookie => $expirationTime ${expirationTime}`);
          } 
          else if (cookie !== undefined && isCookieValid(cookie)){
            console.log(`cookie is still valid => `);
          } 
          next();
      });
      
      async function generateAccessToken() {
          const jwtGenerator = new JwtGenerator();
          const token = jwtGenerator.generateToken("");
          const authRequest = {
              method: "POST",
              uri: "https://login.salesforce.com/services/oauth2/token",
              form: {
                  "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
                  "assertion": token
              }
          }
          try {
              const sfResponse = JSON.parse(await request(authRequest));
              console.log(`sfResponse ${JSON.stringify(sfResponse)}`);
              return sfResponse.access_token;
      
          } catch(err){
              console.log(err);
          }
      }
      
      async function isCookieValid(cookie) {
          var first = cookie.split(';')
          var second = first[1].split('=');
          if(second[1] > moment().format()) {
              return true;
          } else {
              return false;
          }
      }

      async function parseCookies(request) {
        var list = {},
            rc = request.cookies.oauthCookie;
    
        rc && rc.split(';').forEach(function( cookie ) {
            var parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });
        return Object.keys(list)[0];
      }

      app.use(
        "/slds/",
        express.static("./node_modules/@salesforce-ux/design-system/")
      );

      app.use('/', async (req, res, next) => {
        let cookie = req.cookies.oauthCookie;
        let cookieToken;

        if (req.method === 'OPTIONS' || req.method === 'GET') {
            // CORS Preflight
            next();
        } else {
          const targetURL = req.header('salesforceproxy-endpoint');
          if (!targetURL) {
              next();
          }
          
          if(isCookieValid(cookie)) {
            cookieToken = await parseCookies(req);
          } else {
            cookieToken = await generateAccessToken();
          }
  
          request({ url: targetURL, method: req.method, json: req.body, headers: {'Authorization': `Bearer ${cookieToken}`} },
               (error, response, body) => {
                  if (error) {
                      console.error('error: ' + response.statusCode)
                  }
              }).pipe(res);
        }
      });
    },
    // Disable to use standard html routing
    historyApiFallback: true,
    port: 4002,
  },
  resolve: {
    symlinks: false,
    alias: alias,
  },
};
