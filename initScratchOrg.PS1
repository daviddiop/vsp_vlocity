Write-Output '##### CREATING SCRATCH ORG #####'

$t = $host.ui.RawUI.ForegroundColor

$SCRATCH_ALIAS = git rev-parse --abbrev-ref HEAD
$RETURN_CREATE = sfdx force:org:create -f config/project-scratch-def.json -a $SCRATCH_ALIAS -s --json -d 30 -w 60

$PSWD = sfdx force:user:password:generate -u  $SCRATCH_ALIAS --json

##### Updating default record type Business Account Name & DeveloperName #####
#####sfdx force:data:record:update -s RecordType -w "Name='Business Account'" -v "Name='BusinessAccount' DeveloperName='BusinessAccount'" -u $SCRATCH_ALIAS



Write-Output '##### Install Appexchange Vlocity  #####'
sfdx force:package:install -p 04t1J000000mnwJ -w 180 -u $SCRATCH_ALIAS
sfdx force:package:install -p 04t1U000007TtvC -w 10 -u $SCRATCH_ALIAS

Write-Output '##### Install Appexchange GLSendEmail  #####'
sfdx force:package:install -p 04t0o000003YqTM -w 10 -u $SCRATCH_ALIAS

Read-Host -Prompt "Faire les actions manuelles dans la SO  (press any key to continue or CTRL+C  to quit)"
#installation des metatadata du le répertoire "prepush-metadata"
#Write-Output '##### Deploy Prepush Metadata  #####'
#sfdx force:mdapi:deploy -d prepush-metadata -w 30 -l NoTestRun -u $SCRATCH_ALIAS

#installation des metatadata du le répertoire "force-app"
Write-Output '##### PUSHING Source #####'
sfdx force:source:push -f -u $SCRATCH_ALIAS

#Assignation de la PS VSP Modify Alldata à l'utilisateur courant
sfdx force:user:permset:assign -n PS_VSPModifyAllData -u $SCRATCH_ALIAS
sfdx force:user:permset:assign -n vlocity_ins_Permissions -u $SCRATCH_ALIAS

#installation des metatadata du le répertoire "postpush-metadata"
Write-Output  '##### Deploy Post Push Source #####'
sfdx force:mdapi:deploy -d Postpush-metadata -w 30 -l NoTestRun -u $SCRATCH_ALIAS

#Import des données Vlocity - Mise à jour du fichier de propriété
# Récupérer le username
#recréer le fichier de propriété build.properties avec les nouvelles valeur pour avoir :
#sfdx.username = test-dkigowfa5l4r@example.com
#sf.loginUrl = https://test.salesforce.com


#Import des données sample
Write-Output '##### Adding Sample Data #####'
sfdx texei:data:import --inputdir ./sampleData/Data -u $SCRATCH_ALIAS
sfdx texei:data:import --inputdir ./sampleData/DataInit -u $SCRATCH_ALIAS
sfdx texei:data:import --inputdir ./sampleData/DataVSCard -u $SCRATCH_ALIAS
#Importer un compte et son contrat associé
#Mettre à jour les données dans le package Vlocity
#mettre à jour l'id du CarrierContract__c dans les fichiers :
#IMAGE-ET-SON_DataPack.json


#Import des données Vlocity
#https://github.com/vlocityinc/vlocity_build#install-nodejs
vlocity '-sfdx.username' $SCRATCH_ALIAS -job Vlocity\.vlocity\VlocityComponents.yaml refreshVlocityBase
vlocity '-sfdx.username' $SCRATCH_ALIAS -job Vlocity\.vlocity\VlocityComponents.yaml packDeploy
vlocity '-sfdx.username' $SCRATCH_ALIAS -job Vlocity\.vlocity\VlocityComponents.yaml packRetry


$host.ui.RawUI.ForegroundColor = 'Yellow'
Write-Output '##### Informations de Connexion  #####'
sfdx force:org:display -u $SCRATCH_ALIAS
$host.ui.RawUI.ForegroundColor = 'White'
Write-Output '##### OPENING SCRATCH ORG #####'
sfdx force:org:open -p one/one.app -u $SCRATCH_ALIAS
sfdx force:source:tracking:reset -u $SCRATCH_ALIAS


