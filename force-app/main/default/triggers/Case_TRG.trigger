/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 11-30-2021
 * @last modified by  : David Mignane Diop
**/
trigger Case_TRG on Case (before insert) {
    String RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CustomerRequest').getRecordTypeId();
    String contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CustomerRequest').getRecordTypeId();
    List<Contact> contactList = new List<Contact>();
    List<String> contactEmailList = new List<String>();
    List<Case> caseList = new List<Case>();
    for (Case caseRecord : Trigger.new) {
        if(((caseRecord.ContactId == null) && (caseRecord.Status == 'New') && (caseRecord.Origin == 'Adresse e-mail Offre Foyer' || caseRecord.Origin == 'Adresse e-mail Offre Mobile') && caseRecord.RecordTypeId == RecordTypeId)){
            Contact contactRecord  =  new Contact ();
            contactRecord.Email  = caseRecord.SuppliedEmail;
            contactRecord.LastName   = caseRecord.SuppliedName;
            contactRecord.RecordTypeId = contactRecordTypeId;
            contactList.add(contactRecord);
            contactEmailList.add(caseRecord.SuppliedEmail);
        }
    }
    insert contactList;
    System.debug('contactEmailList'+contactEmailList);
    List<Contact> contactListAfter = [SELECT Id,Email FROM Contact WHERE Email IN: contactEmailList];
    for (Case caseRecord : Trigger.new) {
        for(Contact cont :contactListAfter){
            if(cont.Email == caseRecord.SuppliedEmail){
                caseRecord.ContactId = cont.Id;
            }
        }
    }
    System.debug('contactListAfter'+contactListAfter);
}