/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 10-11-2021
 * @last modified by  : David Mignane Diop
**/
trigger GestionErreursEvent_TRG on GestionErreursEvent__e (after insert) {
    GestionLog_UTL.GestionErreurDTO gestionErreurDTO;
    GestionLog_UTL.GestionErreurDTOreponse gestionErreurDTOreponse;
    List<GestionErreurs__c> gestionErreursList = new List<GestionErreurs__c>() ;
    for (GestionErreursEvent__e event : Trigger.new) {
        System.debug('event.reponseJson__c');
        if(event.reponseJson__c !=null){
            gestionErreurDTOreponse = new GestionLog_UTL.GestionErreurDTOreponse(event.Name__c, event.FonctionName__c, event.DateHeureErreur__c, event.NomObjetSalesforce__c, event.CodeErreurFlux__c, event.MessageErreur__c, event.FluxJSONSOAPEnvoye__c, event.StatutDuFlux__c, event.TableRecordId__c, event.reponseJson__c);
            gestionErreursList.add(GestionLog_UTL.instancierGestionLogReponse(gestionErreurDTOreponse));
    } else {
        gestionErreurDTO = new GestionLog_UTL.GestionErreurDTO(event.Name__c, event.FonctionName__c, event.DateHeureErreur__c, event.NomObjetSalesforce__c, event.CodeErreurFlux__c, event.MessageErreur__c, event.FluxJSONSOAPEnvoye__c, event.StatutDuFlux__c, event.TableRecordId__c);
        gestionErreursList.add(GestionLog_UTL.instancierGestionLog(gestionErreurDTO));
    }
    System.debug('event.reponseJson__c2222');
    }
    System.debug('event.reponseJson__c33333');
    insert gestionErreursList;
}