/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-19-2022
 * @last modified by  : David Mignane Diop
**/
global class PurgePJAsset_SCH  implements Schedulable{
    global void execute(SchedulableContext ctx) {  
        PurgePJAsset_BAT batch =new PurgePJAsset_BAT();
        database.executebatch(batch, 1000);	
    }
}