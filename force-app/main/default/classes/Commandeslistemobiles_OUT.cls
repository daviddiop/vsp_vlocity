/**
 * @File	Commandeslistemobiles_OUT.cls
 * @description classe de gestion de l'appel du callApi
 * @author 	Amir BEN SALEM
 * @date 	30/09/2021
 */
public with sharing class Commandeslistemobiles_OUT {
    public static APIResponse_WSO getCommandeslistemobiles(Commandeslistemobiles_WSI Commandeslistemobilesrequete, String methodType,String accessToken){
        Map<String, Type> mapRespClass = new Map<String, Type>();
        //mapRespClass.put('OK',ContratOGI_WSO.class);
        mapRespClass.put('OK',Commandeslistemobiles_OK_WSO.class);
        //mapRespClass.put('KO',ContratOGI_KO_WSO.class);
        mapRespClass.put('KO',Commandeslistemobiles_OK_WSO.class);
        APIResponse_WSO response= new APIResponse_WSO() ;
        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Type', 'application/json; charset=UTF-8');
        String  authorizationHeader = 'Bearer ' + accessToken;
        request.setHeader('authorization', authorizationHeader);
        System.debug('WSO2VSP_CST.NAMED_CREDEBTIAL_COMMANDES_LISTE_MOBILES '+WSO2VSP_CST.NAMED_CREDEBTIAL_COMMANDES_LISTE_MOBILES+'Methode'+methodType+'Request'+request+'Commandeslistemobilesrequete'+Commandeslistemobilesrequete+'mapRespClass'+mapRespClass);
        response = (APIResponse_WSO) WSO2VSP_UTL.callApi(WSO2VSP_CST.NAMED_CREDEBTIAL_COMMANDES_LISTE_MOBILES, methodType,request, Commandeslistemobilesrequete, mapRespClass);
        System.debug('responsetest'+response);

        return response;
    }
}