/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-12-2022
 * @last modified by  : David Mignane Diop
**/
@istest
public with sharing class EnvoyerSMS_SVE_TST {
    @istest
    static void testEnvoieSMS(){
        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';

        String body2 = '{"statusCode" : 200, "status":"Ok","destinataires_valides":["+33544845"],"destinataires_invalides":["+6985875"]}';
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,200, 'OK','Ok', body1, body2);
        String destinataire = '47845878';
        String [] destinataires;
        destinataires =  new String []{destinataire,'478458'};
        //{destinataire,'478458'};
        
        //destinataires.add(destinataire);
        //destinataires.add('478458');
        System.debug('destinataires'+destinataires);
        //destinataires.put(destinataire);


        EnvoyerSMS_WSI envoieSMS  = new EnvoyerSMS_WSI('Bonjour, voici votre lien de paiement : urlr.me/B6543',destinataires,'MONETICO');
        System.debug('envoieSMS'+envoieSMS);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        EnvoyerSMS_SVE.EnvoyerSMS(envoieSMS);
        Test.stopTest();
    }
    @istest
    static void testEnvoieSMSKo(){
        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';

        String body2 = '{"statusCode" : 400, "status":"KO","specification":"https://doc.verspieren.com/spec","code":"400","titre":"requete incorrecte","message":"une erreur"}';
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,400, 'OK','KO', body1, body2);
        
        List <String> destinataires = new List<String>();
        String destinataire = '47845878';
        destinataires.add(destinataire);
        //destinataires.put(destinataire);


        EnvoyerSMS_WSI envoieSMS  = new EnvoyerSMS_WSI('Bonjour, voici votre lien de paiement : urlr.me/B6543',destinataires,'MONETICO');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
         EnvoyerSMS_SVE.EnvoyerSMS(envoieSMS);
        Test.stopTest();
    }
}