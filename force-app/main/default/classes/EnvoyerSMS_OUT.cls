/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-14-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class EnvoyerSMS_OUT {
    public EnvoyerSMS_OUT() {

    }
    public static APIResponse_WSO envoyerSMS(EnvoyerSMS_WSI smsRequete, String methodType,String accessToken){
        Map<String, Type> mapRespClass = new Map<String, Type>();
        mapRespClass.put('OK',EnvoyerSMS_OK_WSO.class);
        mapRespClass.put('KO',EnvoyerSMS_KO_WSO.class);
        APIResponse_WSO response= new APIResponse_WSO() ;
        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Type', 'application/json; charset=UTF-8');
        String  authorizationHeader = 'Bearer ' + accessToken;
        request.setHeader('authorization', authorizationHeader);
        //https://am-rec35.verspieren.com/services/sms/envoyer_sms
        response = (APIResponse_WSO) WSO2VSP_UTL.callApi(WSO2VSP_CST.NAMED_CREDENTIAL_ENVOYER_SMS, methodType,request, smsRequete, mapRespClass);
        System.debug('responsetest'+response);

        return response;

        /*String reponse = getMockResponse('responseSinistre');
        APIResponse_WSO res= new APIResponse_WSO();
        res= (APIResponse_WSO) EnvoyerSMS_OK_WSO.class.newInstance();
        res = (APIResponse_WSO) (JSON.deserialize(reponse, EnvoyerSMS_OK_WSO.class));
        res.status = 'OK';
        res.statusCode = 200;

        //return response;
        return res;*/
    }

    /*public static String getMockResponse( String key )
    { 
        reduireURL__mdt  custMdt = [
            SELECT Id,apiName__c, response__c 
            FROM reduireURL__mdt
            WHERE apiName__c = 'envoieSMS'
        ];
        System.debug('custMdt' +custMdt.response__c);
        String body = custMdt.response__c;
        return body;
    }*/
}