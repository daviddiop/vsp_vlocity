/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-21-2022
 * @last modified by  : David Mignane Diop
**/
global class PurgePJAsset_BAT implements Database.Batchable<sObject>  {
    // Récuperation des records de l'objet attachement avec asset status egal annule
    public Database.QueryLocator start(Database.BatchableContext context)
    {
        String profondeur = Label.ProfondeurPurgePJ;
        integer p = Integer.valueOf(profondeur);
        Date todayP = Date.today().addDays(-p);
        String query;
        query= 'SELECT Id From Asset WHERE CreatedDate < :todayP';

        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext context, List<SObject> records)
    {
        
        /*if (Test.isRunningTest() == True){
            assets = [SELECT Id From Asset];
        }*/
        System.debug('assets'+ records);
        List<EmailMessage> EmailMessageList;
        List<Attachment> attachments;
       
        if(!records.isEmpty()){
            set<Id> ids = new Set<Id>();
            for(SObject ass:records){
                ids.add(ass.Id);
            }
            System.debug('ids'+ids);
            EmailMessageList = [SELECT Id, ParentId, Subject, FromName, FromAddress, RelatedToId FROM EmailMessage WHERE RelatedToId IN:Ids];
        }
        if(!EmailMessageList.isEmpty()){
            set<Id> EmailIds = new Set<Id>();
            for(EmailMessage emailM:EmailMessageList){
                EmailIds.add(emailM.Id);
            }
            attachments = [SELECT id,name,ParentId FROM Attachment WHERE ParentId IN :EmailIds];
        }
        
        
        delete attachments;
    }
    public void finish(Database.BatchableContext BC) {
    system.debug('La purge des pieces jointe est terminée');
    }
}