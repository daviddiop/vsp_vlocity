/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 11-16-2021
 * @last modified by  : David Mignane Diop
**/
public with sharing class RecepValorisationPrestationNES_SVE {
    public class  PrestationNESWrapper {
        public ClaimLineItemDTO[] ClaimLineItem;
    }
    public class ClaimLineItemDTO{
        public String ClaimId;
        public String Ext_RepairClaimLineItem;
        public String ClaimLineItemDetails;
        public String ClaimLineItemClaimant;
        public Decimal AdjustedAmount;
        public String Ext_ClaimId;
        public String ExtBillingDate;
    }
    
    public class reponseOkWrapper{
        public responseOkDTO[] responseOk;
    }

   

    public class responseOkDTO{
        public String status;
        public String erreur;
        public String message;
        public String Ext_ClaimId;
        public String Ext_RepairClaimLineItem;
    }

    public class reponseKoWrapper{
        public responseKoDTO[] responseKo;
    }
    public class responseKoDTO{
        public String status;
        public erreurDTO erreur;
        public String Ext_ClaimId;
        public String Ext_RepairClaimLineItem;
    }

    public class erreurDTO{
        public String erreur_message;
        public String erreur_description;
    }

   /* public static Map<String,String> getClaimLineDetails(){
        Map<String,String> claimDetailsValue = new Map<String,String>();
        claimDetailsValue.put('00','Résolution en ligne client');
        claimDetailsValue.put('01','Cout gestion dossier SAV');
        claimDetailsValue.put('02','Cout gestion dossier Indemnisation');
        claimDetailsValue.put('03','Transport PPE Allée');
        claimDetailsValue.put('04','tation Technique');
        claimDetailsValue.put('05','Transport PPE Retour');
        claimDetailsValue.put('06','Indemnisation Numéraire');
        claimDetailsValue.put('07','Bon d\'achat');
        return claimDetailsValue;
    }*/
    public static Map<String,String> getClaimLineDetailsValue(){
        Map<String,String > pickListValuesList = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__ClaimLineItem__c.ClaimLineItemDetails__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.put(pickListVal.getValue(),pickListVal.getLabel());
        }
        return pickListValuesList;
    }
    public static Map<String,String> getClaimLineClaimant(){
        Map<String,String> claimClaimantValue = new Map<String,String>();
        claimClaimantValue.put('CLI','CLI');
        claimClaimantValue.put('REPA','REPA');
        claimClaimantValue.put('TIER','TIER');
        return claimClaimantValue;
    }

    public static String managePrestationNES(RecepValorisationPrestationNES_SVE.PrestationNESWrapper ClaimLineItem  ){
        List<vlocity_ins__ClaimLineItem__c> claimLineItemRecordList = new List<vlocity_ins__ClaimLineItem__c>();
        List<RecepValorisationPrestationNES_OK_WSO.responseDTO> responseDTOList = new List<RecepValorisationPrestationNES_OK_WSO.responseDTO>();
        String responseDeRetour;
        String flux = 'Reception et valorisation prestation NES';
        Datetime dateHeureErreur = Datetime.now();
        String codeErreurFlux ;
        String messageErreur = '';
        String fluxJsonSoapEnvoye = JSON.serializePretty(ClaimLineItem);
        String statusDuFlux;

            for(RecepValorisationPrestationNES_SVE.ClaimLineItemDTO  claim : ClaimLineItem.ClaimLineItem){
                Claim_QR claimQuery =  new Claim_QR();
                List<Id> claimIds = new List<Id>();
                claimIds.add(claim.ClaimId);
                List<vlocity_ins__InsuranceClaim__c> claimRecords =  claimQuery.getClaimRecord(claimIds);
                vlocity_ins__ClaimCoverage__c claimCoverage =  claimQuery.getClaimCoverage(claim.ClaimId);
                System.debug('claimCoverage'+claimCoverage);
                if(claimCoverage != null){
                    Map<String, Object> inputObj = new Map<String, Object> ();
                    Map<String,String> claimClaimantValue = getClaimLineClaimant();
                    Map<String,String> claimDetailsValue = getClaimLineDetailsValue();
                    inputObj.put('claimId',claim.ClaimId);
                    inputObj.put('claimCoverageId',claimCoverage.Id);
                    inputObj.put('type','Loss');
                    //passer le status a open 
                    //les deux fields ClaimLineItemOgiCustomerId__c / PolicyCodeNumber__c
                    inputObj.put('claimLineItemDetails',claim.ClaimLineItemDetails);
                    inputObj.put('adjustedAmount',claim.AdjustedAmount);
                    inputObj.put('ext_ClaimId',claim.Ext_ClaimId);
                    inputObj.put('claimLineItemClaimant',claim.ClaimLineItemClaimant);
                    inputObj.put('extBillingDate',claim.ExtBillingDate);
                    inputObj.put('ext_RepairClaimLineItem',claim.Ext_RepairClaimLineItem);
                    inputObj.put('status','Open');
                    inputObj.put('claimLineItemOgi',claimRecords[0].vlocity_ins__PrimaryPolicyAssetId__r.Account.Ext_OGICustomerID__pc);
                    if(claimDetailsValue.get(claim.ClaimLineItemDetails) != null){
                        inputObj.put('description',claimDetailsValue.get(claim.ClaimLineItemDetails));
                    }
                    
                    String productCodeMobile = 'ED_CLAIM_ELECTROSUR_MOBILE';
                    String productCode = 'ED_CLAIM_ELECTROSUR';
                    if(claimRecords[0].vlocity_ins__SpecProduct2Id__r.ProductCode == productCodeMobile ){
                        inputObj.put('claimLineNumGarantie','PANNE');
                    } 
                    else if (claimRecords[0].vlocity_ins__SpecProduct2Id__r.ProductCode == productCodeMobile ){
                        inputObj.put('claimLineNumGarantie','CASSE');
                    }
                    inputObj.put('claimLineNumGarantie',claimRecords[0].Tech_PolicyCodeNumber__c);
                    String str = JSON.serialize(inputObj);
                    Map<String, Object> outputObj = new Map<String, Object> ();
                    Map<String, Object> ipOptions = new Map<String, Object> ();
                    String procedureName = 'claim_valorisationPrestationNES';
                    System.debug('test'+inputObj);
                    outputObj = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName,inputObj,ipOptions);
                    System.debug('test'+outputObj);
                    if(outputObj.get('error') === 'OK'){
                        System.debug('ok'+outputObj);
                        RecepValorisationPrestationNES_OK_WSO.responseDTO  reponseOk = new RecepValorisationPrestationNES_OK_WSO.responseDTO();
                        reponseOk = RecepValorisationPrestationNES_OK_WSO.createResponse(outputObj.get('error').toString(),'Ligne de sinistre récupéré avec succès',null,null,claim.Ext_ClaimId,claim.Ext_RepairClaimLineItem);
                        responseDTOList.add(reponseOk);
                        statusDuFlux = 'OK';
                        codeErreurFlux = 'INVOKE-200';
                        messageErreur = 'Ligne de sinistre récupéré avec succès' ;
                        GestionLog_UTL.creerLogwithResponse(new GestionLog_UTL.GestionErreurDTOreponse(flux, 'callIN', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' ',JSON.serialize(reponseOk)));

                    }
                    if(outputObj.get('success') == false){ 
                        if(((Map<String, Object>)outputObj.get('result')).get('error') != 'OK'){
                            RecepValorisationPrestationNES_OK_WSO.responseDTO  reponseKo = new RecepValorisationPrestationNES_OK_WSO.responseDTO();
                            reponseKo = RecepValorisationPrestationNES_OK_WSO.createResponse('KO',null,((Map<String, Object>)outputObj.get('result')).get('errorCode').toString(),((Map<String, Object>)outputObj.get('result')).get('error').toString(),claim.Ext_ClaimId,claim.Ext_RepairClaimLineItem);
                            responseDTOList.add(reponseKo);
                            statusDuFlux = 'KO';
                            codeErreurFlux = 'INVOKE-500';
                            messageErreur = ((Map<String, Object>)outputObj.get('result')).get('errorCode').toString() ;
                            GestionLog_UTL.creerLogwithResponse(new GestionLog_UTL.GestionErreurDTOreponse(flux, 'callIN', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' ',JSON.serialize(reponseKo)));

                        }
                    }
                } else {
                    RecepValorisationPrestationNES_OK_WSO.responseDTO  reponseKo = new RecepValorisationPrestationNES_OK_WSO.responseDTO();
                    reponseKo = RecepValorisationPrestationNES_OK_WSO.createResponse('KO',null,'Not_Found','Sinistre non trouvé ou claim coverage non trouvé',claim.Ext_ClaimId,claim.Ext_RepairClaimLineItem);
                    responseDTOList.add(reponseKo);
                    statusDuFlux = 'KO';
                    codeErreurFlux = 'INVOKE-500';
                    messageErreur = 'Sinistre non trouvé ou claim coverage non trouvé' ;
                    GestionLog_UTL.creerLogwithResponse(new GestionLog_UTL.GestionErreurDTOreponse(flux, 'callIN', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' ','Sinistre non trouvé ou claim coverage non trouvé'));

                    }
                }
                System.debug('retour a la fin '+JSON.serialize(responseDTOList));
                

        return JSON.serialize(responseDTOList);
    }
}