global class ClaimLineItemBatch implements  Database.Batchable<sObject> {
    global List<ID> listClaimIds;
    global ClaimLineItemBatch(List<ID> listClaimIds) {
        this.listClaimIds = listClaimIds;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {  
        // collect the batches of records or objects to be passed to execute
        String queryString = 'select  Id, Ext_AccountingStatus__c from vlocity_ins__ClaimLineItem__c WHERE Id IN :listClaimIds';
        return Database.getQueryLocator(queryString);
    }
    global void execute(Database.BatchableContext bc, List<vlocity_ins__ClaimLineItem__c> scope){
        // process each batch of records
        List<Id> lotIds = new List<Id>();
        for(vlocity_ins__ClaimLineItem__c obj: scope)
        {
            lotIds.add(obj.Id);
        }
        if(!lotIds.isEmpty())
        {
            ID jobID = System.enqueueJob(new ClaimLineQueuebleJob(lotIds));
        }
        
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}