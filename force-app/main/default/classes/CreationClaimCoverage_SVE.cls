/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 12-03-2021
 * @last modified by  : David Mignane Diop
**/
global with sharing class CreationClaimCoverage_SVE {
    @InvocableMethod(label='creationClaimCoverage' description=' creation claim coverage' category='vlocity_ins__InsuranceClaim__c')
    public static void CreationClaimCoverage_SVE(list<Id> claimIds) {
        String procedureName = 'claim_claimCoverage';
        System.debug('creationClaimCoverage++++++++++++++++++++++++');
        for(Id claimId:claimIds){
         	
            CallCreateClaimCoverage(claimId,procedureName);
       
    		}
    }
    public static Map<String,String> getClaimLineDetailsValue(){
        Map<String,String > pickListValuesList = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__ClaimLineItem__c.ClaimLineItemDetails__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.put(pickListVal.getValue(),pickListVal.getLabel());
        }
        return pickListValuesList;
    }
    @future
    public Static void CallCreateClaimCoverage (Id claimIds , String ProcedureName){
        Map<String, Object> outputObj = new Map<String, Object> (); 
        Map<String, Object> inputObj = new Map<String, Object> ();
        Map<String, Object> ipOptions = new Map<String, Object> ();
        List<Id>  claimId = new List<Id>();
        claimId.add(claimIds);
        Claim_QR claimQuery =  new Claim_QR();
        createParty_SVE partyService =new createParty_SVE();
        List<vlocity_ins__InsuranceClaim__c> claimRecord =  claimQuery.getClaimRecord(claimId);
        vlocity_ins__Party__c partyRecord = partyService.createParty(claimRecord[0]);
        vlocity_ins__AssetCoverage__c  assetCoverage = claimQuery.getAssetCoverage(claimRecord[0].vlocity_ins__PrimaryPolicyAssetId__r.Id);
        vlocity_ins__AssetInsuredItem__c  assetInsuredItem = claimQuery.getAssetInsuredItem(claimRecord[0].vlocity_ins__PrimaryPolicyAssetId__r.Id);
        vlocity_ins__InsuranceClaimPartyRelationship__c  insuranceClaimParty = partyService.createClaimPartyRelationship(claimRecord[0],partyRecord.Id);
        
        vlocity_ins__InsuranceClaimInvolvedProperty__c  ClaimInvolvedProperty = partyService.createInsuranceClaimInvolvedProperty(claimRecord[0]);
        vlocity_ins__Party__c partyRecordNes = [SELECT Id,vlocity_ins__PartyEntityName__c FROM  vlocity_ins__Party__c WHERE  vlocity_ins__PartyEntityName__c ='NES'];
        vlocity_ins__PartyRelationshipType__c partyTypeReparateur = [SELECT Id,Name FROM vlocity_ins__PartyRelationshipType__c WHERE Name = 'Reparateur' ];
        vlocity_ins__InsuranceClaimPartyRelationship__c claimPartyRelationshipReparateur = new vlocity_ins__InsuranceClaimPartyRelationship__c();
        claimPartyRelationshipReparateur.Name =  'NES';
        claimPartyRelationshipReparateur.vlocity_ins__PartyId__c = partyRecordNes.Id;
        claimPartyRelationshipReparateur.vlocity_ins__PartyRelationshipType__c = partyTypeReparateur.Id;
        claimPartyRelationshipReparateur.vlocity_ins__ClaimId__c = claimRecord[0].Id;
        //insert claimPartyRelationshipReparateur;
         upsert claimPartyRelationshipReparateur;
        vlocity_ins__Party__c partyRecordPartenaire = [SELECT Id,vlocity_ins__PartyEntityName__c FROM  vlocity_ins__Party__c WHERE  vlocity_ins__PartyEntityName__c ='ELECTRO DEPOT'];
        vlocity_ins__PartyRelationshipType__c partyTypePartenaire = [SELECT Id,Name FROM vlocity_ins__PartyRelationshipType__c WHERE Name = 'Partenaire' ];
        vlocity_ins__InsuranceClaimPartyRelationship__c claimPartyRelationshipPartenaire = new vlocity_ins__InsuranceClaimPartyRelationship__c();
        claimPartyRelationshipPartenaire.Name =  'ELECTRO DEPOT';
        claimPartyRelationshipPartenaire.vlocity_ins__PartyId__c = partyRecordPartenaire.Id;
        claimPartyRelationshipPartenaire.vlocity_ins__PartyRelationshipType__c = partyTypePartenaire.Id;
        claimPartyRelationshipPartenaire.vlocity_ins__ClaimId__c = claimRecord[0].Id;
        //insert claimPartyRelationshipPartenaire;
         upsert claimPartyRelationshipPartenaire;
        vlocity_ins__InsuranceClaimInvolvedInjury__c assetInsuranceClaimInvolved = partyService.createClaimInvolved(claimRecord[0],partyRecord.Id,insuranceClaimParty.Id);
        //vlocity_ins__InsuranceClaimInvolvedInjury__c assetInsuranceClaimInvolved =  claimQuery.getAssetInsuranceClaimInvolved(claimIds);
        //vlocity_ins__InsuranceClaimPartyRelationship__c  insuranceClaimParty = claimQuery.getInsuranceClaimParty(claimIds);
        inputObj.put('claimId',claimIds);
        if(insuranceClaimParty != null){
        inputObj.put('claimantId',insuranceClaimParty.Id);
        } 
        if(assetInsuranceClaimInvolved != null){
        inputObj.put('involvedId',assetInsuranceClaimInvolved.Id);
        } 
        if(assetInsuredItem != null){
        inputObj.put('insuredId',assetInsuredItem.Id);
        } 
        if(assetCoverage != null){
        inputObj.put('assetCoverageId',assetCoverage.Id);
        }
        inputObj.put('reserveAmount',claimRecord[0].Tech_AcceptedRemainingAmount__c);
        //claimRecord[0].Tech_AcceptedRemainingAmount__c
        inputObj.put('reserveProcessingMode','CoverageReserve');
        try 
          {
            
            outputObj = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName,inputObj,ipOptions);

            System.debug('test output reponse apres l appel du service vlocity'+outputObj.get('claimCoverageId'));
            if(claimRecord[0].Type_Indemnisation_Reprise__c == '06' ||claimRecord[0].Type_Indemnisation_Reprise__c == '07'){
            Map<String, Object> inputObjRecord = new Map<String, Object> ();
            Map<String, Object> outputObjRecord = new Map<String, Object> ();
            Map<String,String> claimDetailsValue = getClaimLineDetailsValue();
                    inputObjRecord.put('claimId',claimIds);
                    inputObjRecord.put('claimCoverageId',outputObj.get('claimCoverageId'));
                    inputObjRecord.put('type','Loss');
                    //passer le status a open 
                    //les deux fields ClaimLineItemOgiCustomerId__c / PolicyCodeNumber__c
                    inputObjRecord.put('claimLineItemDetails',claimRecord[0].Type_Indemnisation_Reprise__c);
                    
                    inputObjRecord.put('adjustedAmount',claimRecord[0].ClaimVoucherAmount__c);
                    inputObjRecord.put('ext_ClaimId',claimRecord[0].Ext_ClaimNumber__c);
                    inputObjRecord.put('numSinistreOGI',claimRecord[0].vlocity_ins__RevisedDescription__c);
                    inputObjRecord.put('claimLineNumGarantie',claimRecord[0].Tech_PolicyCodeNumber__c);
                    if(claimRecord[0].Type_Indemnisation_Reprise__c == '06'){
                    inputObjRecord.put('claimLineItemClaimant','CLI');
                    }
                    if(claimRecord[0].Type_Indemnisation_Reprise__c == '07'){
                        inputObjRecord.put('claimLineItemClaimant','TIER');
                        }
                    //inputObjRecord.put('claimLineItemClaimant',claim.ClaimLineItemClaimant);
                    //inputObjRecord.put('extBillingDate',claim.ExtBillingDate);
                    //inputObjRecord.put('ext_RepairClaimLineItem',claim.Ext_RepairClaimLineItem);
                    inputObjRecord.put('status','Open');
                    inputObjRecord.put('claimLineItemOgi',claimRecord[0].vlocity_ins__PrimaryPolicyAssetId__r.Account.Ext_OGICustomerID__pc);
                    if(claimDetailsValue.get(claimRecord[0].Type_Indemnisation_Reprise__c) != null){
                        //inputObj.put('description',claimDetailsValue.get(claim.ClaimLineItemDetails));
                        inputObjRecord.put('description',claimDetailsValue.get(claimRecord[0].Type_Indemnisation_Reprise__c));
                    }
                    
                    String procedureName2 = 'claim_valorisationPrestationNES';
                    outputObjRecord = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName2,inputObjRecord,ipOptions);
                    System.debug('test'+outputObjRecord.get('claimItemId'));
                    System.debug('test'+((Map<String, Object>)outputObjRecord.get('result')).get('error').toString());
                }
          } 
        catch (Exception e)
        {
            System.debug('++++++++ eCause '+ e.getCause());
             
        }
    }
}