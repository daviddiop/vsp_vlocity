/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 11-25-2021
 * @last modified by  : David Mignane Diop
**/
public with sharing class createParty_SVE {
    public vlocity_ins__Party__c createParty(vlocity_ins__InsuranceClaim__c claimRecord){
        vlocity_ins__Party__c partyRecord = new vlocity_ins__Party__c();
        partyRecord.vlocity_ins__AccountId__c = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.Id;
        partyRecord.Name = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.Name;
        insert partyRecord;
        return partyRecord;
    }

    public vlocity_ins__InsuranceClaimPartyRelationship__c createClaimPartyRelationship(vlocity_ins__InsuranceClaim__c claimRecord, String partyId){
         List<vlocity_ins__InsuranceClaimPartyRelationship__c> listRecord = new List<vlocity_ins__InsuranceClaimPartyRelationship__c>();
        vlocity_ins__PartyRelationshipType__c partyTypeClient = [SELECT Id,Name FROM vlocity_ins__PartyRelationshipType__c WHERE Name = 'Client' ];
        vlocity_ins__InsuranceClaimPartyRelationship__c claimPartyRelationshipClient = new vlocity_ins__InsuranceClaimPartyRelationship__c();
        claimPartyRelationshipClient.Name =  claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.Name;
        claimPartyRelationshipClient.vlocity_ins__PartyId__c = partyId;
        claimPartyRelationshipClient.vlocity_ins__PartyRelationshipType__c = partyTypeClient.Id;
        claimPartyRelationshipClient.vlocity_ins__ClaimId__c = claimRecord.Id;
        //insert claimPartyRelationshipClient;
        listRecord.add(claimPartyRelationshipClient);
      
        insert listRecord;
        return claimPartyRelationshipClient;
    }
    
        public vlocity_ins__InsuranceClaimInvolvedProperty__c createInsuranceClaimInvolvedProperty(vlocity_ins__InsuranceClaim__c claimRecord){
        vlocity_ins__InsuranceClaimInvolvedProperty__c ClaimInvolvedProperty =  new  vlocity_ins__InsuranceClaimInvolvedProperty__c();
        ClaimInvolvedProperty.ClaimDeviceFamily__c = claimRecord.ClaimDeviceFamily__c;
        ClaimInvolvedProperty.ClaimDeviceInvoiceDate__c =  claimRecord.ClaimDeviceInvoiceDate__c;
        ClaimInvolvedProperty.ClaimDevicePurchasePos__c = claimRecord.ClaimDevicePurchasePos__c;
        ClaimInvolvedProperty.ClaimDeviceSubFamily__c = claimRecord.ClaimDeviceSubFamily__c;
        ClaimInvolvedProperty.ClaimDeviceType__c = claimRecord.ClaimDeviceType__c;
        ClaimInvolvedProperty.ClaimDeviceModel__c = claimRecord.ClaimDeviceModel__c;
        ClaimInvolvedProperty.vlocity_ins__ClaimId__c = claimRecord.Id;
        insert ClaimInvolvedProperty;
        return ClaimInvolvedProperty;
    }

    public vlocity_ins__InsuranceClaimInvolvedInjury__c createClaimInvolved(vlocity_ins__InsuranceClaim__c claimRecord, String partyId,String ClaimPartyRelationship){
        vlocity_ins__PartyRelationshipType__c partyType = [SELECT Id,Name FROM vlocity_ins__PartyRelationshipType__c WHERE Name = 'Client' ];
        vlocity_ins__InsuranceClaimInvolvedInjury__c ClaimInvolved = new vlocity_ins__InsuranceClaimInvolvedInjury__c();
        ClaimInvolved.Name =  'Bénéficiaire';
        ClaimInvolved.vlocity_ins__ClaimantId__c = ClaimPartyRelationship;
        ClaimInvolved.vlocity_ins__ClaimId__c = claimRecord.Id;
        ClaimInvolved.vlocity_ins__SpecProduct2Id__c = claimRecord.vlocity_ins__SpecProduct2Id__r.Id;
        ClaimInvolved.ClaimantEmail__c = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.PersonEmail;
        ClaimInvolved.ClaimantFirstName__c = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.FirstName;
        ClaimInvolved.ClaimantLastName__c = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.LastName;
        ClaimInvolved.ClaimantPostalCode__c = Decimal.valueOf(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingPostalCode);
        insert ClaimInvolved;
        return ClaimInvolved;
    }

}