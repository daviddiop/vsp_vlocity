@isTest
public class PurgeGestionErreurTest_BAT{
    
@isTest 
    static void check(){
		//création de log sur l'année 2021
        list<GestionErreurs__c> logToInsert =new list<GestionErreurs__c>();
        
        GestionErreurs__c log1 = new GestionErreurs__c();
        log1.DateHeureErreur__c = datetime.now();
        log1.FonctionName__c = 'TESTLOG1';
        logToInsert.add(log1); 
        
        GestionErreurs__c log2 = new GestionErreurs__c();
        log2.DateHeureErreur__c = datetime.now();
        log2.FonctionName__c = 'TESTLOG2';
        logToInsert.add(log2);   
        
        insert logToInsert;
    
        Test.startTest();
    		
        	list<GestionErreurs__c> insertedLog = [select id from GestionErreurs__c];
    		
            PurgeGestionErreur_BAT x = new PurgeGestionErreur_BAT();
            database.executeBatch(x);
        	
        Test.stopTest();
        
        list<GestionErreurs__c> CheckLogsafterDelete = [select id from GestionErreurs__c];
        	
        	System.assert(insertedLog.size()!=CheckLogsafterDelete.size());
        	System.assert(insertedLog.size()==2);
        	System.assert(CheckLogsafterDelete.size()==0);
    
    }
}