/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-14-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class ReduireURL_OUT {
    public ReduireURL_OUT() {

    }
    public static APIResponse_WSO reduireUrl(ReduireURL_WSI urlRequete, String methodType,String accessToken){
        Map<String, Type> mapRespClass = new Map<String, Type>();
        mapRespClass.put('OK',ReduireURL_OK_WSO.class);
        mapRespClass.put('KO',ReduireURL_KO_WSO.class);
        APIResponse_WSO response= new APIResponse_WSO() ;
        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Type', 'application/json; charset=UTF-8');
        String  authorizationHeader = 'Bearer ' + accessToken;
        request.setHeader('authorization', authorizationHeader);
        //https://am-rec35.verspieren.com/services/url/reduire_url
        response = (APIResponse_WSO) WSO2VSP_UTL.callApi(WSO2VSP_CST.NAMED_CREDENTIAL_REDUIRE_URL, methodType,request, urlRequete, mapRespClass);
        System.debug('responsetest'+response);
        return response;
        /*String reponse = getMockResponse('responseSinistre');
        APIResponse_WSO res= new APIResponse_WSO();
        res= (APIResponse_WSO) ReduireURL_OK_WSO.class.newInstance();
        res = (APIResponse_WSO) (JSON.deserialize(reponse, ReduireURL_OK_WSO.class));
        res.status = 'OK';
        res.statusCode = 200;

        
        return res;*/
    }

    /*public static String getMockResponse( String key )
    { 
        reduireURL__mdt  custMdt = [
            SELECT Id,apiName__c, response__c 
            FROM reduireURL__mdt
            WHERE apiName__c = 'urlReduite'
        ];
        System.debug('custMdt' +custMdt.response__c);
        String body = custMdt.response__c;
        return body;
    }*/
}