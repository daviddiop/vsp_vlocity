/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 04-14-2022
 * @last modified by  : David Mignane Diop
**/
public class MandatGeneration_SVE {
    
   /**
     * @description     :   Génration du Mandat PDF en Blob  
     * @author          :   ALAMZABI
     * @date            :   18/03/2021
     * Paramétres: AssetId(Identifiant de l'asset), Iban(Iban Client)
     *             Rum (Numero rum retourné par OGI), ogiContractId (Identifiant du contrat OGI)
     */
    
    public static Blob generateMandatPDF(Id accountId , String iban , String rum , String ogiContractId){
       		
            PageReference pageRef = new PageReference('/apex/MandatPDF_VFP');
       		pageRef.setRedirect(false);
            pageRef.getParameters().put('id', accountId);
        	pageRef.getParameters().put('iban', iban);
        	pageRef.getParameters().put('rum', rum);
        	pageRef.getParameters().put('ogicontract', ogiContractId);
       		
        	Blob PDF;
        	
        	if(Test.isRunningTest()){
				PDF = EncodingUtil.base64Decode('TEST');
			}
        	try{
            	 PDF = pageRef.getContentAsPDF(); 	   
            }
        	catch(Exception e){
            	
        	}
       		return PDF; 
   	}
	   public static Blob generateMandatPDFAsset(Id accountId , String iban , String rum , String ogiContractId,String street, String postalCode, String city, String country){
       		
		PageReference pageRef = new PageReference('/apex/MandatPDFAsset_VFP');
		   pageRef.setRedirect(false);
		pageRef.getParameters().put('id', accountId);
		pageRef.getParameters().put('iban', iban);
		pageRef.getParameters().put('rum', rum);
		pageRef.getParameters().put('ogicontract', ogiContractId);
		pageRef.getParameters().put('street', street);
		pageRef.getParameters().put('postalCode', postalCode);
		pageRef.getParameters().put('city', city);
		pageRef.getParameters().put('country', country);
		   
		Blob PDF;
		
		if(Test.isRunningTest()){
			PDF = EncodingUtil.base64Decode('TEST');
		}
		try{
			 PDF = pageRef.getContentAsPDF(); 	   
		}
		catch(Exception e){
			
		}
		   return PDF; 
   }
    
    
    
}