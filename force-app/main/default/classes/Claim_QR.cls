/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 12-02-2021
 * @last modified by  : David Mignane Diop
**/
public with sharing class Claim_QR {
    public List<vlocity_ins__InsuranceClaim__c> getClaimRecord(List<Id> claimIds){
        List<vlocity_ins__InsuranceClaim__c> claimList = [ SELECT  id,Name,vlocity_ins__Description__c,ClaimDeviceFamily__c,ClaimDevicePurchasePos__c,ClaimDeviceSubFamily__c,
            vlocity_ins__PrimaryPolicyAssetId__r.ProductCategory__c , vlocity_ins__PrimaryPolicyAssetId__r.Tech_ProductUnivers__c,WarrantyType__c,vlocity_ins__RevisedDescription__c,
            ClaimDeviceBrand__c,OtherClaimDeviceBrand__c,Tech_AcceptedRemainingAmount__c,Type_Indemnisation_Reprise__c,Date_ReglementOGI_Reprise__c,Ext_ClaimNumber__c,
            ClaimDeviceModel__c,ClaimDeviceCode__c,	ClaimDeviceInvoiceDate__c,ClaimVoucherAmount__c,ClaimImei__c,ClaimDeviceInvoiceNumber__c,Tech_PolicyCodeNumber__c,
            vlocity_ins__LossDate__c,vlocity_ins__ReportedDate__c,ClaimDeviceAmount__c,ClaimDeviceType__c,vlocity_ins__SpecProduct2Id__r.ProductCode,vlocity_ins__SpecProduct2Id__r.Id,
            vlocity_ins__PrimaryPolicyAssetId__r.Name,vlocity_ins__PrimaryPolicyAssetId__r.Ext_OGIContractID__c,vlocity_ins__PrimaryPolicyAssetId__r.vlocity_ins__EffectiveDate__c,vlocity_ins__PrimaryPolicyAssetId__r.ProductName__c,vlocity_ins__PrimaryPolicyAssetId__r.Account.Name,
            vlocity_ins__PrimaryPolicyAssetId__r.Account.FirstName,vlocity_ins__PrimaryPolicyAssetId__r.Account.LastName,vlocity_ins__PrimaryPolicyAssetId__r.Account.PersonMobilePhone,
            vlocity_ins__PrimaryPolicyAssetId__r.Account.PersonEmail,vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingPostalCode,vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingCountry,
            vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingStreet,vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingCity,vlocity_ins__PrimaryPolicyAssetId__r.Account.Ext_OGICustomerID__pc
            FROM vlocity_ins__InsuranceClaim__c
            WHERE Id in: claimIds
        ];
        if (claimList.isEmpty()) {
            return null;
        }
        return claimList;
    }

    public vlocity_ins__ClaimCoverage__c getClaimCoverage(String claimId){
        List<vlocity_ins__ClaimCoverage__c> claimCoverage = 
        [
            SELECT Id,vlocity_ins__AssetCoverageId__r.vlocity_ins__Product2Id__r.CarrierContract__r.PolicyConvetionNumber__c
             from vlocity_ins__ClaimCoverage__c 
             WHERE vlocity_ins__ClaimId__c   =: claimId 
             LIMIT 1
        ];
        if (claimCoverage.isEmpty()) {
            return null;
        }
        return claimCoverage[0];
    }

    public vlocity_ins__AssetCoverage__c getAssetCoverage(String assetId){
        List<vlocity_ins__AssetCoverage__c> assetCoverage = 
        [
             SELECT Id,vlocity_ins__PolicyAssetId__c
             FROM vlocity_ins__AssetCoverage__c
             WHERE vlocity_ins__PolicyAssetId__c   =: assetId 
             LIMIT 1
        ];
        if (assetCoverage.isEmpty()) {
            return null;
        }
        return assetCoverage[0];
    }

    public vlocity_ins__AssetInsuredItem__c getAssetInsuredItem(String assetId){
        List<vlocity_ins__AssetInsuredItem__c> assetInsuredItem = 
        [
             SELECT Id,vlocity_ins__PolicyAssetId__c
             FROM vlocity_ins__AssetInsuredItem__c
             WHERE vlocity_ins__PolicyAssetId__c   =: assetId 
             LIMIT 1
        ];
        if (assetInsuredItem.isEmpty()) {
            return null;
        }
        return assetInsuredItem[0];
    }

    public vlocity_ins__InsuranceClaimInvolvedInjury__c getAssetInsuranceClaimInvolved(String claimId){
        List<vlocity_ins__InsuranceClaimInvolvedInjury__c> assetInsuranceClaimInvolved = 
        [
             SELECT Id,vlocity_ins__ClaimId__c
             FROM vlocity_ins__InsuranceClaimInvolvedInjury__c
             WHERE vlocity_ins__ClaimId__c   =: claimId 
             LIMIT 1
        ];
        if (assetInsuranceClaimInvolved.isEmpty()) {
            return null;
        }
        return assetInsuranceClaimInvolved[0];
    }
    public vlocity_ins__InsuranceClaimPartyRelationship__c getInsuranceClaimParty(String claimId){
        List<vlocity_ins__InsuranceClaimPartyRelationship__c> InsuranceClaimParty = 
        [
             SELECT Id,vlocity_ins__ClaimId__c
             FROM vlocity_ins__InsuranceClaimPartyRelationship__c
             WHERE vlocity_ins__ClaimId__c   =: claimId 
             LIMIT 1
        ];
        if (InsuranceClaimParty.isEmpty()) {
            return null;
        }
        return InsuranceClaimParty[0];
    }
}