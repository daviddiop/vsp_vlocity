/**
 * Created by Amir BEN SALEM on 30.09.2021.
 */
public with sharing class Commandeslistemobiles_OK_WSO extends APIXMLResponse_WSO{
    public String id_commande;
    public Commandeslistemobiles_WSO.Client client;
    public Commandeslistemobiles_WSO.Commande commande;
    public string code_message;
    public string libelle_court_erreur;
    public string libelle_long_erreur;
    public string libelle_technique;
    public string type_message;
    public Fault fault;

    public Commandeslistemobiles_OK_WSO() {
        super();
     }
    /*  public Class Client{
        public String id_client_partenaire;
        public Naissance naissance;
        public Identite identite;
        public Adresse_postale adresse_postale;
        public ContactObj contactObj;
    }
    public Class Naissance{
        public String date;
        public String pays;
        public String departement;
        public String ville;
        public String code_postal;
    }
    public Class Identite{
        public String prenom;
        public String nom;
        public String civilite;
    }
    public Class Adresse_postale{
        public String rue;
        public String complement_rue;
        public String code_postal;
        public String ville;
        public String pays;
    }
    public Class ContactObj{
        public String email;
        public String telephone_fixe;
        public String telephone_mobile;
    }
  /*   public Class TransactionParameters{
        public String orderChannel;
        public String orderId;
    } 
  
    public class Commande{
        public Ligne[] lignes;
    }
    public class Ligne{
       public String quantite;
       public String montant_net_ttc;
       public ProduitMobile produit;
       public String[] produits_assurance_casse_mobile;

    }
    public class ProduitMobile{
        public String reference;
        public String emei;
        public String marque;
        public String modele;
        public String etat;
    } */
    public class Fault {
        public String code ;
        public String message;
        public String description ;
        public String type;
    }
}