/**
 * @File        :  Commandeslistemobiles_SVE.cls
 * @description : Methodes pour retourner les produits : offre mobile web
 * author       :   Amir BEN SALEM
 * @date        :   30/09/2021
 *
 */
public with sharing class Commandeslistemobiles_SVE {
    
    /**
       * @description : Methodes de service de gestion de la reception des produits liée à l'Authentification
       * @param  Commandeslistemobiles_WSI
       * @return  :   APIResponse_WSO
    */
    public static APIResponse_WSO createCommandeslistemobiles(Commandeslistemobiles_WSI request) {
        Map<String, Object> responseAuth = WSO2VSP_SVE.getToken();
        APIResponse_WSO response;
	
        if (!((Boolean) responseAuth.get('erreur'))) {
            System.debug('responseAuth' + (String) responseAuth.get('token'));
            response = creerCommandeslistemobilesRequest(request, (String) responseAuth.get('token'));
        }
        return response;
    }

    /***
     * @description Methodes de service de reception de la liste des mobiles liée à l'Authentification
     * @param Commandeslistemobiles_WSI,token
     * @return Object APIResponse_WSO
     * @exception aucune
     */
    public static APIResponse_WSO creerCommandeslistemobilesRequest(Commandeslistemobiles_WSI request, String token) {
        System.debug('token' + token);
        System.debug('requestrequest' + request);
        String flux = 'Commandes Liste Mobiles WSO2';
        Datetime dateHeureErreur = Datetime.now();
        String codeErreurFlux ;
        String messageErreur = '';
        String fluxJsonSoapEnvoye = JSON.serializePretty(request);
        String statusDuFlux;
        APIResponse_WSO response;
        Try {
            APIResponse_WSO apiResponseWSO = Commandeslistemobiles_OUT.getCommandeslistemobiles(request,'GET', token);
            System.debug('apiResponseWSO--here'+apiResponseWSO);
            Commandeslistemobiles_OK_WSO CommandeslistemobilesOkResponse = (Commandeslistemobiles_OK_WSO) apiResponseWSO;
            System.debug('CommandeslistemobileskResponse'+CommandeslistemobilesOkResponse);
                        if(CommandeslistemobilesOkResponse.statusCode == 200){
             
                response = apiResponseWSO;
                System.debug('Amir return response'+response);
                statusDuFlux = 'Ok';
                codeErreurFlux = String.valueOf(CommandeslistemobilesOkResponse.statusCode);
                System.debug('Amir codeErreurFlux'+codeErreurFlux);
                messageErreur = 'liste des produits mobiles retournée avec succes  ' ;
            } else {
                System.debug('ELSE');  
                 Commandeslistemobiles_KO_WSO commandeslistemobiles = new Commandeslistemobiles_KO_WSO(CommandeslistemobilesOkResponse.code_message, CommandeslistemobilesOkResponse.libelle_court_erreur, CommandeslistemobilesOkResponse.libelle_long_erreur, CommandeslistemobilesOkResponse.libelle_technique, CommandeslistemobilesOkResponse.type_message);
                commandeslistemobiles.statusCode = CommandeslistemobilesOkResponse.statusCode;
                commandeslistemobiles.status = CommandeslistemobilesOkResponse.status; 
                response = apiResponseWSO;
                statusDuFlux = 'Erreur';
                codeErreurFlux = String.valueOf(CommandeslistemobilesOkResponse.statusCode);

               // messageErreur = CommandeslistemobilesOkResponse.libelle_court_erreur;
            }
            /*if (apiResponseWSO.status == 'OK') {
                ContratOGI_WSO contractOGIOk = (ContratOGI_WSO) apiResponseWSO;
                System.debug('contractOGIOk'+contractOGIOk);
                ContratOGI_WSO contratOGI = new ContratOGI_WSO(contractOGIOk.num_client, contractOGIOk.num_contrat, contractOGIOk.num_quittance, contractOGIOk.rum);
                contratOGI.statusCode = contractOGIOk.statusCode;
                contratOGI.status = contractOGIOk.status;
                response = apiResponseWSO;
                statusDuFlux = 'Ok';
                codeErreurFlux = String.valueOf(contractOGIOk.statusCode);
                if (contractOGIOk.fault != null) {
                    messageErreur = contractOGIOk.fault.description ;
                }

            } else {
                ContratOGI_KO_WSO contractOGIKo = (ContratOGI_KO_WSO) apiResponseWSO;
                System.debug('contractOGIKo'+contractOGIKo);
                ContratOGI_KO_WSO contratOGI = new ContratOGI_KO_WSO(contractOGIKo.code_message, contractOGIKo.libelle_court_erreur, contractOGIKo.libelle_long_erreur, contractOGIKo.libelle_technique, contractOGIKo.type_message);
                contratOGI.statusCode = contractOGIKo.statusCode;
                contratOGI.status = contractOGIKo.status;
                response = apiResponseWSO;
                statusDuFlux = 'Erreur';
                codeErreurFlux = String.valueOf(contractOGIKo.statusCode);
                messageErreur = contractOGIKo.libelle_long_erreur;
            }*/
            String logId = GestionLog_UTL.creerLog(new GestionLog_UTL.GestionErreurDTO(flux, 'callApi', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' '));
        } catch (Exception e) {
            System.debug('************ exception ' + e.getMessage());
            String logId = GestionLog_UTL.creerLog(new GestionLog_UTL.GestionErreurDTO('ExceptionCallApexFromOmniscript1', 'AException', Datetime.now(), ' ', 'EXCP', 'Exception: '+ e, e.getStackTraceString(), 'KO', ' '));

        }
        return response;
    }

}