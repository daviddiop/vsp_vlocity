public class MandatPDFAsset_CTL {
    
    /**
     * @description     :   MandatPDF Asset VisualForce Page Controler
     * @author          :   ALAMZABI
     * @date            :   13/04/2022
     * Paramétres: 
	*/
    
    private final Account account;
    
    private final String street;
    private final String postalCode;
    private final String city;
    private final String country;
    
    private final String iban;
    private final String ogiContract;
    private final String rum;

    
 
    public MandatPDFAsset_CTL() {
        
        account = [SELECT Id, Name, LastName,FirstName FROM Account 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        if(ApexPages.currentPage().getParameters().get('assetid') != null){
        	Asset asset = [SELECT id, vlocity_ins__BillingStreet__c,vlocity_ins__BillingPostalCode__c,vlocity_ins__BillingCity__c,
                   vlocity_ins__BillingCountry__c From Asset where Id = :ApexPages.currentPage().getParameters().get('assetid')];
        	
            street = asset.vlocity_ins__BillingStreet__c;
            city = asset.vlocity_ins__BillingCity__c;
            postalCode = asset.vlocity_ins__BillingPostalCode__c;
            country = asset.vlocity_ins__BillingCountry__c;
        }
        else {
            street = ApexPages.currentPage().getParameters().get('street');
            city = ApexPages.currentPage().getParameters().get('city');
            postalCode = ApexPages.currentPage().getParameters().get('postalCode');
            country = ApexPages.currentPage().getParameters().get('country');
        }
        
        iban = ApexPages.currentPage().getParameters().get('iban');
        ogiContract = ApexPages.currentPage().getParameters().get('ogicontract');
        rum = ApexPages.currentPage().getParameters().get('rum');
        
    }
 
    public Account getAccount() {
        return account;
    }
    
    public String getStreet() {
        return street;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getPostalCode() {
        return postalCode;
    }
    
    public String getCountry() {
        return country;
    }
    
    public String getIban() {
        return iban;
    }
    
    public String getOgiContract() {
        return ogiContract;
    }
	
    public String getRum() {
        return rum;
    }
}