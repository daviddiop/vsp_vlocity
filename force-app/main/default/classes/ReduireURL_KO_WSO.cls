/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-12-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class ReduireURL_KO_WSO extends APIXMLResponse_WSO {
    public string specification;
    public string code;
    public string titre;
    public string message;
    public Fault fault ;
    
    public ReduireURL_KO_WSO(){}

    public ReduireURL_KO_WSO(string  specification,string code,string titre, string message) {
        this.specification = specification;
        this.code = code;
        this.titre = titre;
        this.message = message;
    }
    public class Fault {
        public String code ;
        public String message;
        public String description ;
        public String type;

    }
}