/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 04-29-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class EnvoyerSMS_SVE {
    public static APIResponse_WSO EnvoyerSMS(EnvoyerSMS_WSI request) {
        Map<String, Object> responseAuth = WSO2VSP_SVE.getToken();
        APIResponse_WSO response;
	
        if (!((Boolean) responseAuth.get('erreur'))) {
            System.debug('responseAuth' + (String) responseAuth.get('token'));
            response = envoieSmsRequest(request, (String) responseAuth.get('token'));
        }
        return response;
    }
    public static APIResponse_WSO envoieSmsRequest(EnvoyerSMS_WSI request, String token) {
        
        String flux = 'Envoyer sms avec short link';
        Datetime dateHeureErreur = Datetime.now();
        String codeErreurFlux ;
        String messageErreur = '';
        String fluxJsonSoapEnvoye = JSON.serializePretty(request);
        String statusDuFlux;
        String reponseJson;
        APIResponse_WSO response;
        Try {
            APIResponse_WSO apiResponseWSO = EnvoyerSMS_OUT.envoyerSMS(request, 'POST', token);
            System.debug('test'+request);
            if (apiResponseWSO.status == 'OK') {
                EnvoyerSMS_OK_WSO envoieSmsOk = (EnvoyerSMS_OK_WSO) apiResponseWSO;
                EnvoyerSMS_OK_WSO smsRequete = new EnvoyerSMS_OK_WSO(envoieSmsOk.destinataires_valides,envoieSmsOk.destinataires_invalides);
                smsRequete.statusCode = envoieSmsOk.statusCode;
                smsRequete.status = envoieSmsOk.status;
                response = apiResponseWSO;
                statusDuFlux = 'Ok';
                codeErreurFlux = String.valueOf(smsRequete.statusCode);
                reponseJson = JSON.serialize(smsRequete)  ;
                System.debug('reponseJson'+reponseJson );


            } else {
                EnvoyerSMS_KO_WSO envoieSmsKo = (EnvoyerSMS_KO_WSO) apiResponseWSO;
                EnvoyerSMS_KO_WSO smsRequete = new EnvoyerSMS_KO_WSO(envoieSmsKo.specification, envoieSmsKo.code, envoieSmsKo.titre, envoieSmsKo.message);
                smsRequete.statusCode = envoieSmsKo.statusCode;
                smsRequete.status = envoieSmsKo.status;
                response = apiResponseWSO;
                statusDuFlux = 'Erreur';
                codeErreurFlux = String.valueOf(smsRequete.statusCode);
                reponseJson = JSON.serialize(smsRequete)  ;

            }
            String logId = GestionLog_UTL.creerLogwithResponse(new GestionLog_UTL.GestionErreurDTOreponse(flux, 'callApi', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' ',reponseJson));

            //String logId = GestionLog_UTL.creerLog(new GestionLog_UTL.GestionErreurDTO(flux, 'callApi', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' '));
        } catch (Exception e) {
            String logId = GestionLog_UTL.creerLogwithResponse(new GestionLog_UTL.GestionErreurDTOreponse(flux, 'callApi', dateHeureErreur, ' ', codeErreurFlux, e.getMessage(), fluxJsonSoapEnvoye, statusDuFlux, ' ',reponseJson));

            System.debug('************ exception ' + e.getMessage());
            System.debug('************ getStackTraceString() ' + e.getStackTraceString());
            System.debug('************ exception ' + e.getCause());
            
        }
        return response;
    }
}