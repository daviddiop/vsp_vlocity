public class ClaimLineQueuebleJob  implements Queueable {
 private List<ID> lstClaimIds;
    
    public ClaimLineQueuebleJob(List<ID> lstClaimIds){
        this.lstClaimIds = lstClaimIds;
    }
     public void execute(QueueableContext context) { 
         List<vlocity_ins__ClaimLineItem__c> listClaimLineItems = new List<vlocity_ins__ClaimLineItem__c>();
          if(!lstClaimIds.isEmpty()){
            String procedureName = 'cliamLine_sinistre';
            for(ID claimLineItemId : lstClaimIds){
               String res = ClaimPaymentFuture.CallClaimPayment(claimLineItemId,procedureName);
               listClaimLineItems.add(new vlocity_ins__ClaimLineItem__c(Id = claimLineItemId , TECH_ResponseOGI__c = res));
            }
              update listClaimLineItems;
            //ClaimPaymentFuture.CallClaimPaymentTest(lstClaimIds,procedureName);
        }
    }
}