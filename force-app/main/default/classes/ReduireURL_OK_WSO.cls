/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-10-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class ReduireURL_OK_WSO extends APIXMLResponse_WSO{
    public string url_reduite;
    public ReduireURL_OK_WSO() {}
    public ReduireURL_OK_WSO(String url) {
        this.url_reduite = url;
    }
}