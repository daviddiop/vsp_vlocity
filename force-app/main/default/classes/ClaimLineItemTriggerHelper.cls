public without sharing class ClaimLineItemTriggerHelper {
    public ClaimLineItemTriggerHelper() {
        System.debug('Inside AccountTriggerHelper Constructor');
    }
    public void CreateClaim(List<vlocity_ins__ClaimLineItem__c> lstClaimLineItems) {
        System.debug('Inside Task 1');
        System.debug('tester si le flow a été lancer');
      	ID jobID = System.enqueueJob(new ClaimLineQueuebleJob(lstClaimLineItems));
    }
    public void doTask2() {
        System.debug('Inside Task 2');
    }
    public void doTask3() {
        System.debug('Inside Task 3');
    }
    public void doTask4() {
        System.debug('Inside Task 4');
    }
    public void doTask5() {
        System.debug('Inside Task 5');
    }

}