/**
 * @author Amir BEN SALEM (Salesforce)
 * @date 07.10.2021
 * @description classe de test pour Commandeslistemobiles_SVE
 */
@isTest
public with sharing class Commandeslistemobiles_SVE_TST {
    @isTest
    static void testCreerCommandeslistemobilesKo(){

        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';


        String body2 = '{"statusCode" : 401, "status":"Erreur","type_message":null,"libelle_technique":null,"libelle_long_erreur":null,"libelle_court_erreur":null, "fault":{"message":"Invalid Credentials","description":"Access failure for API: /factures_mobiles/1.0.0, version: 1.0.0 status: (900901) - Invalid Credentials. Make sure you have given the correct access token","code":"900901"},"code_message":null}';
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,401, 'OK','Erreur', body1, body2);
        Test.setMock(HttpCalloutMock.class, mock);
         String id_facture='F924 7778435-21/001';
         String id_magasin='924';
         String nom_client='Biden';

         Commandeslistemobiles_WSI commandeslistemobilesWSI = new Commandeslistemobiles_WSI(id_facture,id_magasin,nom_client);

        Test.startTest();
        Commandeslistemobiles_SVE.createCommandeslistemobiles(commandeslistemobilesWSI);
        Test.stopTest();
    }
     @isTest
    static void testCreerDevisOk(){

        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';


        String body2 = '{"statusCode" : 200, "status":"OK","id_facture":"F924 7778435-21/001","id_magasin":"924","nom_client":"Biden", "fault":{"message":"Invalid Credentials","description":"Access failure for API: /factures_mobiles/1.0.0, version: 1.0.0 status: (900901) - Invalid Credentials. Make sure you have given the correct access token","code":"900901"},"code_message":null}';
        //WSO2VSP_MCK mock = new WSO2VSP_MCK(401, 'Erreur', body1, body2);
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,200, 'OK','OK', body1, body2);
        Test.setMock(HttpCalloutMock.class, mock);
        String id_facture='F924 7778435-21/001';
        String id_magasin='924';
        String nom_client='Biden';
        Commandeslistemobiles_WSI commandeslistemobilesWSI = new Commandeslistemobiles_WSI(id_facture,id_magasin,nom_client);
        Test.startTest();
        Commandeslistemobiles_SVE.createCommandeslistemobiles(commandeslistemobilesWSI);
        Test.stopTest();
    } 
}