/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 04-29-2022
 * @last modified by  : David Mignane Diop
**/
@isTest
public class MandatPDFAsset_TST {

    @isTest static void testGetAccount(){    
       
       Account acc = Account_TDF.getPersonAccount();    
       insert acc;
       Asset ass = Asset_TDF.getAsset(acc.id,'TestAsset','03');
       insert ass;
       String iban = ''; 
       String rum = 'RUM09091XX1';
       String contractOGI = 'OGI1989981';
       System.debug('ass'+ass.Id);
        
       Test.StartTest(); 
       
       PageReference mandatPage = Page.MandatPDFAsset_VFP;     
       mandatPage.getParameters().put('id', String.valueOf(acc.Id));
       mandatPage.getParameters().put('iban', iban);
       mandatPage.getParameters().put('ogicontract', contractOGI);
       mandatPage.getParameters().put('rum', rum);
       mandatPage.getParameters().put('assetid', String.valueOf(ass.Id)); 
       Test.setCurrentPage(mandatPage);
          
       MandatPDFAsset_CTL controler = new MandatPDFAsset_CTL();         
        
       Account result = controler.getAccount();
       String street = controler.getStreet();
       String city = controler.getCity();
       String country = controler.getCountry();
       String postalCode = controler.getPostalCode();
       //Asset resultAsset = controler.getAsset();

       System.assertEquals(result.id, acc.id);
       //System.assertEquals(resultAsset.id, ass.id); 
       System.assertEquals(controler.getIban(), iban);
       System.assertEquals(controler.getRum(), rum);
       System.assertEquals(controler.getOgiContract(), contractOGI);
        
   }
   @isTest static void testGetAccountKo(){    
       
    Account acc = Account_TDF.getPersonAccount();    
    insert acc;
    Asset ass = Asset_TDF.getAsset(acc.id,'TestAsset','03');
    insert ass;
    String iban = ''; 
    String rum = 'RUM09091XX1';
    String contractOGI = 'OGI1989981';
    System.debug('ass'+ass.Id);
     
    Test.StartTest(); 
    
    PageReference mandatPage = Page.MandatPDFAsset_VFP;     
    mandatPage.getParameters().put('id', String.valueOf(acc.Id));
    mandatPage.getParameters().put('iban', iban);
    mandatPage.getParameters().put('ogicontract', contractOGI);
    mandatPage.getParameters().put('rum', rum);
    //mandatPage.getParameters().put('assetid', String.valueOf(ass.Id)); 
    Test.setCurrentPage(mandatPage);
       
    MandatPDFAsset_CTL controler = new MandatPDFAsset_CTL();         
     
    Account result = controler.getAccount();
    //Asset resultAsset = controler.getAsset();

    System.assertEquals(result.id, acc.id);
    //System.assertEquals(resultAsset.id, ass.id); 
    System.assertEquals(controler.getIban(), iban);
    System.assertEquals(controler.getRum(), rum);
    System.assertEquals(controler.getOgiContract(), contractOGI);
     
}
    
}