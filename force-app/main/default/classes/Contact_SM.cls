/**
 * @description       : Contact Service Manager
 * @author            : LAMZABI Achraf
 * @group             : 
 * @last modified on  : 12-16-2021
 * @last modified by  : LAMZABI Achraf
**/


public class Contact_SM {
	
        /**
     * @description       : Get duplicated Contact records 
     * @author            : LAMZABI Achraf
     * @group             : 
     * @last modified on  : 12-16-2021
     * @last modified by  : LAMZABI Achraf
    **/

    public static List<Contact> findContacts(List<Contact> cons) {
	List<Contact> foundContacts = new List<Contact>();
	List<Datacloud.FindDuplicatesResult> results;
	try {
		results = Datacloud.FindDuplicates.findDuplicates(cons);
	} catch (Exception ex) {
		return null;
	}

	// Loop the original contacts
	for (Integer i = 0; i < cons.size(); i++) {
		Contact foundCon = null;
		// Find the first duplicate result with a match result, then use the first match record.
		for (Datacloud.DuplicateResult dr : results[i].getDuplicateResults()) {
			if (dr.matchResults.size() > 0 && dr.getMatchResults()[0].matchRecords.size() > 0) {
				foundCon = (Contact) dr.getMatchResults()[0].getMatchRecords()[0].getRecord();
				break;
			}
		}
		if(foundCon != null) foundContacts.add(foundCon);
	}
	system.debug('aaaaa:' + foundContacts);
	return foundContacts;
}
    
}