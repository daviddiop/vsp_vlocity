/**
 * @author Amir BEN SALEM (Salesforce)
 * @date 30.09.2021
 * @description classe d'instantiation  de l'objet Commandeslistemobiles_WSO, structure de la response ok du Web service Commandeslistemobiles (offre mobile web)
 */

public with sharing class Commandeslistemobiles_WSO extends APIXMLResponse_WSO{
    public String id_commande;
    public Client client;
    public Commande commande;
    public Fault fault ;
    
    public Commandeslistemobiles_WSO() {}
    public Commandeslistemobiles_WSO(String id_commande,Client client) {
     this.id_commande=id_commande;
      this.client=client;/*
     this.commande=commande; */
    }
    
    /* public Class MerchantData{
        public CustomerInformation customerInformation;
        public TransactionParameters transactionParameters;
        public CaddieObj caddie;
    } */
    public Class Client{
        public String id_client_partenaire;
        public Naissance naissance;
        public Identite identite;
        public Adresse_postale adresse_postale;
        public ContactObj contact;
        public Client() {}
    }
    public Class Naissance{
        public String date_naissance;
        public String pays;
        public String departement;
        public String ville;
        public String code_postal;
    }
    public Class Identite{
        public String prenom;
        public String nom;
        public String civilite;
    }
    public Class Adresse_postale{
        public String rue;
        public String complement_rue;
        public String code_postal;
        public String ville;
        public String pays;
    }
    public Class ContactObj{
        public String email;
        public String telephone_fixe;
        public String telephone_mobile;
    }
 /*    public Class TransactionParameters{
        public String orderChannel;
        public String orderId;
    } */
  
    public class Commande{
        public Ligne[] lignes;
    }
    public class Ligne{
       public String quantite;
       public String montant_net_ttc;
       public String reference;
       public String imei;
       public String marque;
       public String modele;
       public String etat;
       public String[] produits_assurance_casse_mobile;

    }
    public class ProduitMobile{
        public String reference;
        public String emei;
        public String marque;
        public String modele;
        public String etat;
    }

   /*  public class TechnicalData{
        public String httpResponse;
        public String responseMessage;
    } */
    public class Fault {
        public String code ;
        public String message;
        public String description ;
        
    }
}