/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-19-2022
 * @last modified by  : David Mignane Diop
**/
@isTest 
 class PurgePJAssetTest_SCH {
    public static testMethod void testschedule() {
        Account accountRecord = new Account();
        accountRecord.Name = 'Business';
        insert accountRecord;
        Asset asset = new Asset();
        asset.Tech_identifiantProcedureYousign__c = '/procedures/123AAA' ;
        asset.Name = '123AAA' ;
        asset.Status = '08';
        asset.ReasonNote__c = 'commentaires';
        asset.RelatedReason__c = '	Renonciation';
        asset.AccountId = accountRecord.Id ;
        //asset.CreatedDate = (Datetime) '2021-03-15' ;
        Date myDate = Date.newInstance(2021, 3, 15);
        asset.dateConfirmationSignatureMandatYousign__c = myDate; //(Datetime) '2021-03-15' ;
        asset.RUM__c = '500109397' ;
        asset.identifiantFichierMandatSignYousign__c = '22222CCCC' ;
        insert asset;

        //Insert emailmessage for ASSET
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.org';
        email.Incoming = True;
        email.ToAddress= 'test@xyz.org';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.RelatedToId = asset.Id; 
        insert email;

        Attachment attach=new Attachment();   	
    	attach.Name='Test';
    	Blob bodyBlob=Blob.valueOf('Testing Body of Attachment');
    	attach.body=bodyBlob;
        attach.parentId=email.id;
        insert attach;
		Test.StartTest();
		PurgePJAsset_SCH testsche = new PurgePJAsset_SCH();
        String sch = '0 0 23 * * ?';
		system.schedule('Test status Check', sch , testsche );
		Test.stopTest();
	}
}