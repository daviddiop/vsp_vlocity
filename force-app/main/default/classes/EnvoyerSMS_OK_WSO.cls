/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-12-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class EnvoyerSMS_OK_WSO extends APIXMLResponse_WSO{
    public String[] destinataires_valides;
    public String[] destinataires_invalides;
    public EnvoyerSMS_OK_WSO() {}
    public EnvoyerSMS_OK_WSO(String[] destinataires_valides,String[] destinataires_invalides) {
        this.destinataires_valides = destinataires_valides;
        this.destinataires_invalides = destinataires_invalides;
    }
}