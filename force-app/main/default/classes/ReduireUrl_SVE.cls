/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-12-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class ReduireUrl_SVE {
    /*ReduireURL_WSI test  = new ReduireURL_WSI();
ReduireURL_WSI.suiviDTO  tesDTO =  new ReduireURL_WSI.suiviDTO();
tesDTO.equipe = 'DPAS'; 
tesDTO.dossier  = 'Electro-Depot';
test.url_a_reduire = 'https://urlr.stoplight.io/docs/urlr/b3A6MTA5MjkxMzM-reduce-a-link';
test.suivi = tesDTO;*/

    public static APIResponse_WSO reduireUrl(ReduireURL_WSI request) {
        Map<String, Object> responseAuth = WSO2VSP_SVE.getToken();
        APIResponse_WSO response;
	
        if (!((Boolean) responseAuth.get('erreur'))) {
            System.debug('responseAuth' + (String) responseAuth.get('token'));
            response = reduireUrlRequest(request, (String) responseAuth.get('token'));
        }
        return response;
    }
    public static APIResponse_WSO reduireUrlRequest(ReduireURL_WSI request, String token) {
        
        String flux = 'Reduire url';
        Datetime dateHeureErreur = Datetime.now();
        String codeErreurFlux ;
        String messageErreur = '';
        String fluxJsonSoapEnvoye = JSON.serializePretty(request);
        String statusDuFlux;
        String reponseJson;
        APIResponse_WSO response;
        Try {
            APIResponse_WSO apiResponseWSO = ReduireURL_OUT.reduireUrl(request, 'POST', token);
            System.debug('test'+request);
            if (apiResponseWSO.status == 'OK') {
                ReduireURL_OK_WSO urlReduiteOk = (ReduireURL_OK_WSO) apiResponseWSO;
                ReduireURL_OK_WSO urlReduite = new ReduireURL_OK_WSO(urlReduiteOk.url_reduite);
                urlReduite.statusCode = urlReduiteOk.statusCode;
                urlReduite.status = urlReduiteOk.status;
                response = apiResponseWSO;
                statusDuFlux = 'Ok';
                codeErreurFlux = String.valueOf(urlReduite.statusCode);
                reponseJson = JSON.serialize(urlReduite.url_reduite);
                System.debug('reponseJson'+reponseJson + '444'+ urlReduite.url_reduite);


            } else {
                System.debug('urlReduiteKo'+apiResponseWSO);
                ReduireURL_KO_WSO urlReduiteKo = (ReduireURL_KO_WSO) apiResponseWSO;
                ReduireURL_KO_WSO urlReduite = new ReduireURL_KO_WSO(urlReduiteKo.specification, urlReduiteKo.code, urlReduiteKo.titre, urlReduiteKo.message);
                urlReduite.statusCode = urlReduiteKo.statusCode;
                urlReduite.status = urlReduiteKo.status;
                response = apiResponseWSO;
                statusDuFlux = 'Erreur';
                System.debug('urlReduiteKo.titre '+urlReduiteKo.titre );
                System.debug('urlReduiteKo.fault.description '+urlReduiteKo.fault.description);
                 if(urlReduiteKo.fault.message != null){
                    messageErreur = urlReduiteKo.fault.description;
                }
                codeErreurFlux = String.valueOf(urlReduite.statusCode);
                reponseJson = JSON.serialize(urlReduite);


            }
            String logId = GestionLog_UTL.creerLogwithResponse(new GestionLog_UTL.GestionErreurDTOreponse(flux, 'callApi', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' ',reponseJson));

            //String logId = GestionLog_UTL.creerLog(new GestionLog_UTL.GestionErreurDTO(flux, 'callApi', dateHeureErreur, ' ', codeErreurFlux, messageErreur, fluxJsonSoapEnvoye, statusDuFlux, ' '));
        } catch (Exception e) {
            String logId = GestionLog_UTL.creerLogwithResponse(new GestionLog_UTL.GestionErreurDTOreponse(flux, 'callApi', dateHeureErreur, ' ', codeErreurFlux, e.getMessage(), fluxJsonSoapEnvoye, statusDuFlux, ' ',reponseJson));

            System.debug('************ exception ' + e.getMessage());
            System.debug('************ getStackTraceString() ' + e.getStackTraceString());
            System.debug('************ exception ' + e.getCause());
            
        }
        return response;
    }
}