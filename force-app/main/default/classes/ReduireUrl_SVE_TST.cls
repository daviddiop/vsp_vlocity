/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-12-2022
 * @last modified by  : David Mignane Diop
**/
@istest
public with sharing class ReduireUrl_SVE_TST {
    @istest
    static void testreduireUrl(){
        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';

        String body2 = '{"statusCode" : 200, "status":"Ok","url_reduite":null}';
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,200, 'OK','Ok', body1, body2);
        
        ReduireURL_WSI.suiviDTO  tesDTO =  new ReduireURL_WSI.suiviDTO();
        tesDTO.equipe = 'DPAS'; 
        tesDTO.dossier  = 'Electro-Depot';
        String url_a_reduire = 'https://urlr.stoplight.io/docs/urlr/b3A6MTA5MjkxMzM-reduce-a-link';

        ReduireURL_WSI urlreduite  = new ReduireURL_WSI(url_a_reduire,tesDTO);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        ReduireUrl_SVE.reduireUrl(urlreduite);
        Test.stopTest();
    }

    @istest
    static void testreduireUrlKo(){
        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';

        String body2 = '{"statusCode" : 400, "status":"KO","specification":"https://doc.verspieren.com/spec","code":"400","titre":"requete incorrecte","message":"une erreur"}';
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,400, 'OK','KO', body1, body2);
        
        ReduireURL_WSI.suiviDTO  tesDTO =  new ReduireURL_WSI.suiviDTO();
        tesDTO.equipe = 'DPAS'; 
        tesDTO.dossier  = 'Electro-Depot';
        String url_a_reduire = 'https://urlr.stoplight.io/docs/urlr/b3A6MTA5MjkxMzM-reduce-a-link';

        ReduireURL_WSI urlreduite  = new ReduireURL_WSI(url_a_reduire,tesDTO);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        ReduireUrl_SVE.reduireUrl(urlreduite);
        Test.stopTest();
    }
}