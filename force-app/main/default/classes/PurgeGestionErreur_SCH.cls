global class PurgeGestionErreur_SCH implements Schedulable {

	global void execute(SchedulableContext ctx) {  
	
        PurgeGestionErreur_BAT batch =new PurgeGestionErreur_BAT();
        database.executebatch(batch, 1000);	
    }
}