global class PurgeGestionErreur_BAT implements Database.Batchable<sObject> {

	// Récuperation de la profondeur choisie 
	static String profondeur = Label.ProfondeurPurge;
	static integer p = Integer.valueOf(profondeur);
    static Date todayP = Date.today().addDays(-p);
	
	// Récuperation des records de l'objet GestionErreurs__c avec une date anteririeur a 1 mois 
    public Database.QueryLocator start(Database.BatchableContext context)
    {
		string query='Select ID from GestionErreurs__c where CreatedDate < :todayP';
        //Pour la classe de test
        if(Test.isRunningTest() == True){
            query='Select ID from GestionErreurs__c';
        }
        return Database.getQueryLocator(query);
    }
	
    public void execute(Database.BatchableContext context, List<SObject> records)
    {
		
        delete records;
    }
	public void finish(Database.BatchableContext BC) {
	system.debug('La purge des gestions erreur est terminée');
	}
}