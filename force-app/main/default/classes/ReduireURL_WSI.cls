/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-10-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class ReduireURL_WSI extends APIRequest_WSI{
    public String url_a_reduire;
    public suiviDTO suivi;
    public ReduireURL_WSI(String url,suiviDTO suivi) {
        this.url_a_reduire = url;
        this.suivi = suivi;
    }

    public class suiviDTO{
        public String equipe;
        public String dossier;
    }
}