/**
 * Created by DAVID DIOP on 21.06.2021.
 */

public with sharing class ImpressionTicket_ITF {
    @InvocableMethod(label='ImprissionTicket' description='impression de ticket' category= 'Asset')
    public static void imprimerTicket(list<Id> assetIds) {

        imprimerTicketFuture(assetIds);
    }
    @future(callout=true)
    public static void imprimerTicketFuture(list<Id> assetIds) {
        List<Asset> assets = [
                select id, Name,ProductCode, StoreCodeNumber__c,ExtMerchantTransactionId__c,CreatedDate,ProductCategory__c,Ext_OGIContractID__c,vlocity_ins__EffectiveDate__c,
                vlocity_ins__BillingStreet__c,vlocity_ins__BillingPostalCode__c,vlocity_ins__BillingCity__c
                from Asset
                where id in :assetIds
        ];

        for (Asset asst : assets) {
            String identPartenaire;
            String  identMagasin;
            String dateSubscription;
            if(asst.ProductCode== Label.ProductCodeBL){
                identPartenaire = 'BOULANGER';
                identMagasin = 'BL'+asst.StoreCodeNumber__c;
                dateSubscription = String.valueOf(asst.vlocity_ins__EffectiveDate__c).replaceAll('-','');
            } else {
                identPartenaire = 'ELECDEPOT';
                identMagasin = 'EL'+asst.StoreCodeNumber__c;
            } 
            System.debug('dateSubscription'+dateSubscription);
            ImpressionTicketCaisse_WSI impressionTicket;
            ImpressionTicketCaisseED_WSI impressionTicketED;
            if(asst.ProductCode== Label.ProductCodeBL){
             impressionTicket = new ImpressionTicketCaisse_WSI (asst.Id,'FR',identPartenaire,'',identMagasin,(String) asst.CreatedDate.format('yyyy-MM-dd'),asst.ExtMerchantTransactionId__c,'OK',asst.ProductCategory__c,asst.Ext_OGIContractID__c,dateSubscription,asst.vlocity_ins__BillingStreet__c,asst.vlocity_ins__BillingPostalCode__c,asst.vlocity_ins__BillingCity__c);
            } else {
            impressionTicketED = new ImpressionTicketCaisseED_WSI (asst.Id,'FR',identPartenaire,'',identMagasin,(String) asst.CreatedDate.format('yyyy-MM-dd'),asst.ExtMerchantTransactionId__c,'OK');
            }
            System.debug('test ImpressionTicketCaisse_WSI');
            System.debug('test ImpressionTicketCaisse_WSI'+asst.CreatedDate.format('yyyy-MM-dd'));
            System.debug('test ProductCategory__c'+asst.ProductCategory__c);
            System.debug('test Ext_OGIContractID__c'+asst.Ext_OGIContractID__c);
            System.debug('test vlocity_ins__EffectiveDate__c'+dateSubscription);
            System.debug('test vlocity_ins__BillingStreet__c'+asst.vlocity_ins__BillingStreet__c);
            System.debug('test vlocity_ins__BillingPostalCode__c'+asst.vlocity_ins__BillingPostalCode__c);
            System.debug('test impressionTicket'+impressionTicket);
            System.debug('test impressionTicketED'+impressionTicketED);
            if(asst.ProductCode== Label.ProductCodeBL){
                ImpressionTicketCaisse_SVE.imprimmerTicketCaisse(impressionTicket);
               } else {
                ImpressionTicketCaisse_SVE.imprimmerTicketCaisse(impressionTicketED);
               }
            //ImpressionTicketCaisse_SVE.imprimmerTicketCaisse(impressionTicket);
        }
    }
}