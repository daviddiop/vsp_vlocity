/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 09-21-2021
 * @last modified by  : David Mignane Diop
**/
@isTest
public  class CreationSinistreNes_SVE_TST {
    @isTest
    static void testCreationSinistreKo(){
        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';


        String body2 = '{"statusCode" : 401, "status":"Erreur","type_message":null,"libelle_technique":null,"libelle_long_erreur":null,"libelle_court_erreur":null, "fault":{"message":"Invalid Credentials","description":"Access failure for API: /ogiSubscription/1.0.0, version: 1.0.0 status: (900901) - Invalid Credentials. Make sure you have given the correct access token","code":"900901"},"code_message":null}';

        //WSO2VSP_MCK mock = new WSO2VSP_MCK(401, 'Erreur', body1, body2);
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,401, 'OK','Erreur', body1, body2);
        Test.setMock(HttpCalloutMock.class, mock);

        WSO2VSPDataFactory_TST.buildAccountObject();
        Account account = [SELECT Id, PersonContactId FROM Account WHERE FirstName ='testSendEmail'];
        account.PersonMobilePhone = '687845787';
        account.PersonEmail = 'd@gmail.com';
        account.BillingStreet = 'rue 50 gr';
        account.BillingCity = 'ville';
        account.BillingPostalCode = '45745';
        account.BillingCountry = 'France';
        upsert account;
        vlocity_ins__InsuranceClaim__c claims = WSO2VSPDataFactory_TST.buildClaimObject();

        List<Asset>  asst =  WSO2VSPDataFactory_TST.buildAssetObject();
        Asset ast = asst[0];
        ast.AccountId = account.Id;
        ast.ProductCategory__c = 'BL';
        Date myDate = Date.newInstance(2020, 2, 17);
        ast.vlocity_ins__EffectiveDate__c = myDate;
        insert ast;

        claims.vlocity_ins__PrimaryPolicyAssetId__c = ast.Id;
        claims.ClaimDeviceFamily__c = 	'ELECTRO-MÉNAGER';
        //claims.ClaimDeviceType__c = 	'AMPLI';
        //claims.ClaimDeviceBrand__c = 'ACER';
        claims.ClaimDeviceCode__c = '47854';
        claims.ClaimDeviceModel__c = '47854';
        claims.ClaimDeviceAmount__c = 4000;
        claims.ClaimDeviceInvoiceNumber__c = '5847';
        claims.Tech_AcceptedRemainingAmount__c = 584;
        claims.vlocity_ins__Description__c = 'description';
        Date myDate2 = Date.newInstance(2020, 2, 17);
        claims.vlocity_ins__LossDate__c = myDate2;
        claims.vlocity_ins__ReportedDate__c = myDate2;


        insert claims;
        List<Id> claimIds = new List<Id>();
        claimIds.add(claims.Id);

        vlocity_ins__ClaimCoverage__c claimCoverage = WSO2VSPDataFactory_TST.buildClaimCovarageObject();
        claimCoverage.vlocity_ins__ClaimId__c = claims.Id;
        insert claimCoverage;
        CreationSinistresNES_SVE.creerSinistre(claimIds);
    }

    @isTest
    static void testCreationSinistreOk(){
        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';


        String body2 = '{"statusCode" : 200, "status":"OK","id_partenaire":"NES","id_sinistre_partenaire":"123456"}';

        //WSO2VSP_MCK mock = new WSO2VSP_MCK(401, 'Erreur', body1, body2);
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,200, 'OK','OK', body1, body2);
        Test.setMock(HttpCalloutMock.class, mock);
        
            WSO2VSPDataFactory_TST.buildAccountObject();
            Account account = [SELECT Id, PersonContactId FROM Account WHERE FirstName ='testSendEmail'];
        account.PersonMobilePhone = '687845787';
        account.PersonEmail = 'd@gmail.com';
        account.BillingStreet = 'rue 50 gr';
        account.BillingCity = 'ville';
        account.BillingPostalCode = '45745';
        account.BillingCountry = 'France';
        upsert account;
            vlocity_ins__InsuranceClaim__c claims = WSO2VSPDataFactory_TST.buildClaimObject();

           List<Asset>  asst =  WSO2VSPDataFactory_TST.buildAssetObject();
           Asset ast = asst[0];
           ast.AccountId = account.Id;
           ast.ProductCategory__c = 'BL';
        Date myDate = Date.newInstance(2020, 2, 17);
           ast.vlocity_ins__EffectiveDate__c = myDate;
           insert ast;
           
           claims.vlocity_ins__PrimaryPolicyAssetId__c = ast.Id;
           claims.ClaimDeviceFamily__c = 	'ELECTRO-MÉNAGER';
           //claims.ClaimDeviceType__c = 	'AMPLI';
           //claims.ClaimDeviceBrand__c = 'ACER';
           claims.ClaimDeviceCode__c = '47854';
           claims.ClaimDeviceModel__c = '47854';
           claims.ClaimDeviceAmount__c = 4000;
           claims.ClaimDeviceInvoiceNumber__c = '5847';
           claims.Tech_AcceptedRemainingAmount__c = 584;
           claims.vlocity_ins__Description__c = 'description';
        Date myDate2 = Date.newInstance(2020, 2, 17);
           claims.vlocity_ins__LossDate__c = myDate2;
           claims.vlocity_ins__ReportedDate__c = myDate2;


           insert claims;
           List<Id> claimIds = new List<Id>();
           claimIds.add(claims.Id);

           vlocity_ins__ClaimCoverage__c claimCoverage = WSO2VSPDataFactory_TST.buildClaimCovarageObject();
           claimCoverage.vlocity_ins__ClaimId__c = claims.Id;
           insert claimCoverage;
        CreationSinistresNES_SVE.creerSinistre(claimIds);
    }
    
    @isTest
    static void testCreationSinistreOk2(){
        String body1 = '{"access_token": "f39e2f48-5646-31e3", "scope": "am_application_scope default",\n'+
                ' "tokenType": "Bearer", "expires_in": 3600  }';


        String body2 = '{"statusCode" : 200, "status":"OK","id_partenaire":"NES","id_sinistre_partenaire":"123456"}';

        //WSO2VSP_MCK mock = new WSO2VSP_MCK(401, 'Erreur', body1, body2);
        WSO2VSP_MCK mock = new WSO2VSP_MCK(200,200, 'OK','OK', body1, body2);
        Test.setMock(HttpCalloutMock.class, mock);
        
            WSO2VSPDataFactory_TST.buildAccountObject();
            Account account = [SELECT Id, PersonContactId FROM Account WHERE FirstName ='testSendEmail'];
        account.PersonMobilePhone = '687845787';
        account.PersonEmail = 'd@gmail.com';
        account.BillingStreet = '1 rue de courcelles de la republique de la tour effel';
        account.BillingCity = 'ville';
        account.BillingPostalCode = '45745';
        account.BillingCountry = 'France';
        upsert account;
            vlocity_ins__InsuranceClaim__c claims = WSO2VSPDataFactory_TST.buildClaimObject();

           List<Asset>  asst =  WSO2VSPDataFactory_TST.buildAssetObject();
           Asset ast = asst[0];
           ast.AccountId = account.Id;
           ast.ProductCategory__c = 'BL';
        Date myDate = Date.newInstance(2020, 2, 17);
           ast.vlocity_ins__EffectiveDate__c = myDate;
           insert ast;
           
           claims.vlocity_ins__PrimaryPolicyAssetId__c = ast.Id;
           claims.ClaimDeviceFamily__c = 	'ELECTRO-MÉNAGER';
           //claims.ClaimDeviceType__c = 	'AMPLI';
           //claims.ClaimDeviceBrand__c = 'ACER';
           claims.ClaimDeviceCode__c = '47854';
           claims.ClaimDeviceModel__c = '47854';
           claims.ClaimDeviceAmount__c = 4000;
           claims.ClaimDeviceInvoiceNumber__c = '5847';
           claims.Tech_AcceptedRemainingAmount__c = 584;
           claims.vlocity_ins__Description__c = 'description';
        Date myDate2 = Date.newInstance(2020, 2, 17);
           claims.vlocity_ins__LossDate__c = myDate2;
           claims.vlocity_ins__ReportedDate__c = myDate2;


           insert claims;
           List<Id> claimIds = new List<Id>();
           claimIds.add(claims.Id);

           vlocity_ins__ClaimCoverage__c claimCoverage = WSO2VSPDataFactory_TST.buildClaimCovarageObject();
           claimCoverage.vlocity_ins__ClaimId__c = claims.Id;
           insert claimCoverage;
        CreationSinistresNES_SVE.creerSinistre(claimIds);
    }
}