/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 05-16-2022
 * @last modified by  : David Mignane Diop
**/
Global class CallApexFromOmniScript_UTL implements vlocity_ins.VlocityOpenInterface2 {

    private static final String CREER_DEVIS = 'CreerDevis';
    private static final String SIGN_ELEC = 'SignatureElectronique';
    private static final String ACTIVATION_CONTRAT = 'ActivationContrat';//todo
    private static final String GETURL = 'GetUrl';
    private static final String IMPRESSIONTICKET = 'ImpressionTicket';
    private static final String FACTURESMOBILES = 'getFacturesMobiles';
    private static final String REDUIREURL = 'reduireUrl';
    private static final String ENVOIESMS = 'envoyerSms';
	private static final String ENVOIMAILPRECONTRACT = 'envoimailprecontract';
    // performance
    private static final String CREATEUPDATEQUOTE = 'createUpdateQuote';
    private static final String UPDATEPOLICY = 'updatePolicy';
    
    

    global Object invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        try {
            if (methodName == CREER_DEVIS) {
                String JsonString = JSON.serialize(inputMap);
                System.debug('JsonString');
                System.debug(JsonString);
                ContratOGI_WSI request = (ContratOGI_WSI) JSON.deserialize(JsonString, ContratOGI_WSI.class);
                APIResponse_WSO response = (APIResponse_WSO) ContratOGI_SVE.createDevis(request);
                ContratOGI_OK_WSO contractOGIOkResponse = (ContratOGI_OK_WSO) response;
                Map<String, Object> outputMapSF = new Map<String, Object>();
                if(String.isNotBlank(contractOGIOkResponse.num_quittance) && String.isNotBlank(contractOGIOkResponse.num_client) &&
                        String.isNotBlank(contractOGIOkResponse.num_contrat)){
                    outputMapSF.put('num_client', contractOGIOkResponse.num_client);
                    outputMapSF.put('num_contrat', contractOGIOkResponse.num_contrat);
                    outputMapSF.put('num_quittance', contractOGIOkResponse.num_quittance);
                    outputMapSF.put('rum', contractOGIOkResponse.rum);
                    outputMapSF.put('jour_prelevement', contractOGIOkResponse.jour_prelevement);
                } else {
                    outputMapSF.put('code_message', contractOGIOkResponse.code_message);
                    outputMapSF.put('libelle_court_erreur', contractOGIOkResponse.libelle_court_erreur);
                    outputMapSF.put('libelle_long_erreur', contractOGIOkResponse.libelle_long_erreur);
                    outputMapSF.put('libelle_technique', contractOGIOkResponse.libelle_technique);
                    outputMapSF.put('type_message', contractOGIOkResponse.type_message);
                }
                outputMap.put('response', outputMapSF);

            }

            else if (methodName == SIGN_ELEC) {
                System.debug('inputMap');
                System.debug(inputMap);
                /*String nom = (String) inputMap.get('nom');
                String prenom = (String) inputMap.get('prenom');*/
                String street = '';
                String postalCode = '';
                String city = '';
                String country = '';
                String accountId = (String) inputMap.get('accountId');
                String iban = (String) inputMap.get('iban');
                String email = (String) inputMap.get('email');
                String numContratOgi = (String) inputMap.get('num contrat ogi');
                String telephone = (String) inputMap.get('telephone');
                Integer rum = Integer.valueOf(inputMap.get('rum'));
                street = (String) inputMap.get('street');
                postalCode = (String) inputMap.get('postalCode');
                 city = (String) inputMap.get('city');
                 country = (String) inputMap.get('country');
                
                SignatureElectronique_WSO reponse = (SignatureElectronique_WSO) SignatureElectronique_SVE.creerProcedureSignatureElectronique(accountId, iban, email, numContratOgi, telephone, rum,street,postalCode,city,country);
                System.debug('JsonResp');
                System.debug(JSON.serialize(reponse));

                Map<String, object> outputMapSF = new Map<String, object>();
                if (reponse.status == 'Ok') {
                    outputMapSF.put('status_signature', reponse.status_signature);
                    outputMapSF.put('identifiant_procedure', reponse.identifiant_procedure);
                    outputMapSF.put('url_signature', reponse.url_signature);
                } else {
                    List<SignatureElectronique_WSO.ErreurDTO> reponseKo = (List<SignatureElectronique_WSO.ErreurDTO>) reponse.erreurs;
                    outputMapSF.put('identifiant_erreur', reponseKo[0].identifiant_erreur);
                    outputMapSF.put('libelle_erreur', reponseKo[0].libelle_erreur);
                    outputMapSF.put('erreur_date_time', reponseKo[0].erreur_date_time);
                }
                outputMap.put('response', outputMapSF);

            } else if (methodName == ACTIVATION_CONTRAT) {

            }

            else if (methodName == GETURL) {
                Double montant = (Double) inputMap.get('montant');
                Map<String, String> inputMapString = new Map<String, String>();
                inputMapString.put('reference', (String) inputMap.get('reference'));
                inputMapString.put('texteLibre', (String) inputMap.get('teste_libre'));
                inputMapString.put('dateCommande', (String) inputMap.get('date_commande'));
                inputMapString.put('civilite', (String) inputMap.get('civilite'));
                inputMapString.put('nom', (String) inputMap.get('nom'));
                inputMapString.put('prenom', (String) inputMap.get('prenom'));
                inputMapString.put('adresseLigne1', (String) inputMap.get('adresse_ligne_1'));
                inputMapString.put('adresseLigne2', (String) inputMap.get('adresse_ligne_2'));
                inputMapString.put('adresseLigne3', (String) inputMap.get('adresse_ligne_3'));
                inputMapString.put('ville', (String) inputMap.get('ville'));
                inputMapString.put('codePostal', (String) inputMap.get('code_postal'));
                inputMapString.put('pays', (String) inputMap.get('pays'));
                inputMapString.put('email', (String) inputMap.get('email'));

                APIResponse_WSO response = (APIResponse_WSO) UrlPaiementMonetico_SVE.creerUrlPaiementMonetico (inputMapString, montant);
                Map<String, Object> outputMapSF = new Map<String, Object>();
                if (response.status == 'OK') {
                    UrlPaiementMonetico_WSO urlPaiementMoneticoOk = (UrlPaiementMonetico_WSO) response;
                    outputMapSF.put('url_paiement', urlPaiementMoneticoOk.url_paiement);
                } else {
                    UrlPaiementMonetico_KO_WSO urlPaiementMoneticoKo = (UrlPaiementMonetico_KO_WSO) response;
                    outputMapSF.put('code_message', urlPaiementMoneticoKo.code_message);
                    outputMapSF.put('libelle_court_erreur', urlPaiementMoneticoKo.libelle_court_erreur);
                    outputMapSF.put('libelle_long_erreur', urlPaiementMoneticoKo.libelle_long_erreur);
                    outputMapSF.put('libelle_technique', urlPaiementMoneticoKo.libelle_technique);
                    outputMapSF.put('type_message', urlPaiementMoneticoKo.type_message);
                }
                outputMap.put('response', outputMapSF);

            }

            else if (methodName == IMPRESSIONTICKET) {
                String identifiantRequete = (String) inputMap.get('identifiant_requete');
                String pays_magasin = (String) inputMap.get('pays_magasin');
                String identifiant_partenaire = (String) inputMap.get('identifiant_partenaire');
                String identifiant_transaction_partenaire = (String) inputMap.get('identifiant_transaction_partenaire');
                String identifiant_magasin = (String) inputMap.get('identifiant_magasin');
                String date_transaction_magasin = (String) inputMap.get('date_transaction_magasin') ;
                String identifiant_transaction_magasin = (String) inputMap.get('identifiant_transaction_magasin');
                String status_transaction = (String) inputMap.get('status_transaction');
                String code_offre = (String) inputMap.get('code_offre');
                String num_contrat = (String) inputMap.get('num_contrat');
                String date_souscription = (String) inputMap.get('date_souscription');
                String adresse = (String) inputMap.get('adresse');
                String code_postal = (String) inputMap.get('code_postal');
                String ville = (String) inputMap.get('ville');

                APIResponse_WSO response = (APIResponse_WSO) ImpressionTicketCaisse_SVE.creerTicketCaisse (identifiantRequete, pays_magasin, identifiant_partenaire, identifiant_transaction_partenaire, identifiant_magasin, date_transaction_magasin, identifiant_transaction_magasin,status_transaction,code_offre,num_contrat,date_souscription,adresse,code_postal,ville);
                Map<String, Object> outputMapSF = new Map<String, Object>();
                if (response.status == 'OK') {
                    ImpressionTicketCaisse_WSO impressionTicketCaisseOk = (ImpressionTicketCaisse_WSO) response;
                    outputMapSF.put('code_reponse', impressionTicketCaisseOk.code_reponse);
                    outputMapSF.put('message_reponse', impressionTicketCaisseOk.message_reponse);
                    outputMapSF.put('status_transaction', impressionTicketCaisseOk.status_transaction);
                } else {
                    ImpressionTicketCaisse_KO_WSO impressionTicketCaisseKo = (ImpressionTicketCaisse_KO_WSO) response;
                    outputMapSF.put('code_reponse', impressionTicketCaisseKo.code_reponse);
                    outputMapSF.put('message_reponse', impressionTicketCaisseKo.message_reponse);
                    outputMapSF.put('status_transaction', impressionTicketCaisseKo.status_transaction);
                }
                outputMap.put('response', outputMapSF);
            }

            else if (methodName == FACTURESMOBILES) {
                String id_facture = (String) inputMap.get('id_facture');
                String id_magasin = (String) inputMap.get('id_magasin');
                String nom_client  = (String) inputMap.get('nom_client');
                System.debug('id_facture'+id_facture+'id_magasin'+id_magasin+'nom_client'+nom_client);
                String JsonString = JSON.serialize(inputMap);
                System.debug('JsonString'+JsonString);
                Commandeslistemobiles_WSI request = (Commandeslistemobiles_WSI) JSON.deserialize(JsonString, Commandeslistemobiles_WSI.class);
               // System.debug('request Amir'+request);
                APIResponse_WSO response = (APIResponse_WSO) Commandeslistemobiles_SVE.createCommandeslistemobiles(request);
                System.debug('before null exc'); 
                Commandeslistemobiles_OK_WSO commandeslistemobilesOk = (Commandeslistemobiles_OK_WSO) response;
                System.debug('response Amir'+commandeslistemobilesOk);               
                Map<String, Object> outputMapSF = new Map<String, Object>();
                if (response.status == 'OK') {
                    System.debug('Body code message'+commandeslistemobilesOk.code_message);
                    outputMapSF.put('id_commande', commandeslistemobilesOk.id_commande);
                    outputMapSF.put('civilite', commandeslistemobilesOk.client.identite.civilite);
                    outputMapSF.put('firstName', commandeslistemobilesOk.client.identite.prenom);
                    outputMapSF.put('lastName', commandeslistemobilesOk.client.identite.nom);
                    outputMapSF.put('dateNaissance', commandeslistemobilesOk.client.naissance.date_naissance);
                    outputMapSF.put('telephone_mobile', commandeslistemobilesOk.client.contact.telephone_mobile); 
                    outputMapSF.put('email', commandeslistemobilesOk.client.contact.email);
                    outputMapSF.put('rue', commandeslistemobilesOk.client.adresse_postale.rue);
                    If(commandeslistemobilesOk.id_commande!=null && commandeslistemobilesOk.commande.lignes.size()>0){
                    outputMapSF.put('code_message', commandeslistemobilesOk.statusCode);
                    }
                    else{
                    outputMapSF.put('code_message', '800');
                    }
                    outputMapSF.put('code_postal', commandeslistemobilesOk.client.adresse_postale.code_postal);
                    outputMapSF.put('ville', commandeslistemobilesOk.client.adresse_postale.ville);
                    outputMapSF.put('LignesProducts', JSON.serialize(commandeslistemobilesOk.commande.lignes));
                         System.debug('Lignes Produits'+commandeslistemobilesOk.commande.lignes);
                         System.debug('Lignes Produits'+commandeslistemobilesOk.commande.lignes.size());
               
                } else {
                    Commandeslistemobiles_KO_WSO commandeslistemobilesKo = (Commandeslistemobiles_KO_WSO) response;
                    outputMapSF.put('code_message', commandeslistemobilesKo.code_message);
                    outputMapSF.put('libelle_court_erreur', commandeslistemobilesKo.libelle_court_erreur);
                    outputMapSF.put('libelle_technique', commandeslistemobilesKo.libelle_technique);
                    outputMapSF.put('libelle_long_erreur', commandeslistemobilesKo.libelle_long_erreur);
                    outputMapSF.put('type_message', commandeslistemobilesKo.type_message);
                }
                outputMap.put('response', outputMapSF);
            }
            else if (methodName == REDUIREURL) {
                String url_a_reduire = (String) inputMap.get('url_a_reduire');
                String equipe = (String) inputMap.get('equipe');
                String dossier  = (String) inputMap.get('dossier');
                ReduireURL_WSI.suiviDTO suivi  = new  ReduireURL_WSI.suiviDTO();
                suivi.equipe = equipe ;
                suivi.dossier = dossier;

                ReduireURL_WSI request = new ReduireURL_WSI(url_a_reduire,suivi);
                APIResponse_WSO response = (APIResponse_WSO) ReduireUrl_SVE.reduireUrl(request);
                Map<String, Object> outputMapSF = new Map<String, Object>();
                if (response.status == 'OK') {
                    ReduireURL_OK_WSO urlOK = (ReduireURL_OK_WSO) response;
                    outputMapSF.put('url_reduite', urlOK.url_reduite);
                } else {
                    ReduireURL_KO_WSO urlKO = (ReduireURL_KO_WSO) response;
                    outputMapSF.put('specification', urlKO.specification);
                    outputMapSF.put('code', urlKO.code);
                    outputMapSF.put('titre', urlKO.titre);
                    outputMapSF.put('message', urlKO.message);
                }
                outputMap.put('response', outputMapSF);
            }

            else if (methodName == ENVOIESMS) {
                String message = (String) inputMap.get('message');
                String destinataire = (String) inputMap.get('destinataire');
                String expediteur  = (String) inputMap.get('expediteur');
                String[] destinataires =  new List<String>();

                //%StepCustomerIdentificationOS02:BlckIdentityPersonOS02:PhoneCofidisCustomerPhoneNumberOS02%

                destinataires.add(destinataire);
                EnvoyerSMS_WSI request = new EnvoyerSMS_WSI(message,destinataires,expediteur);

                APIResponse_WSO response = (APIResponse_WSO) EnvoyerSMS_SVE.EnvoyerSMS(request);
                Map<String, Object> outputMapSF = new Map<String, Object>();
                if (response.status == 'OK') {
                    EnvoyerSMS_OK_WSO envoieSmsOK = (EnvoyerSMS_OK_WSO) response;
                    outputMapSF.put('destinataires_valides', envoieSmsOK.destinataires_valides);
                    outputMapSF.put('destinataires_invalides', envoieSmsOK.destinataires_invalides);
                } else {
                    EnvoyerSMS_KO_WSO envoieSmsKO = (EnvoyerSMS_KO_WSO) response;
                    outputMapSF.put('specification', envoieSmsKO.specification);
                    outputMapSF.put('code', envoieSmsKO.code);
                    outputMapSF.put('titre', envoieSmsKO.titre);
                    outputMapSF.put('message', envoieSmsKO.message);
                }
                outputMap.put('response', outputMapSF);
            }
			
            else if (methodName == ENVOIMAILPRECONTRACT) {
               
                /*GLSendEmail.SendEmail.InvokeSendEmail request = new GLSendEmail.SendEmail.InvokeSendEmail();
                request.EmailTemplateName = (String) inputMap.get('EmailTemplateName');
                request.ToAddresses = (String) inputMap.get('ToAddresses');
                request.TreatTargetObjectAsRecipient = (Boolean) inputMap.get('TreatTargetObjectAsRecipient');
                request.ContactOrLeadID = (String) inputMap.get('ContactOrLeadID');
                request.RelatedToId = (String) inputMap.get('RelatedToId');
                request.OrgWideEmailAddress = (String) inputMap.get('OrgWideEmailAddress');
                
                GLSendEmail.SendEmail.SendEmail(new List<GLSendEmail.SendEmail.InvokeSendEmail>{request});*/
                String EmailTemplateName = (String) inputMap.get('EmailTemplateName');
                String ToAddresses = (String) inputMap.get('ToAddresses');
                Boolean TreatTargetObjectAsRecipient = (Boolean) inputMap.get('TreatTargetObjectAsRecipient');
                String ContactOrLeadID = (String) inputMap.get('ContactOrLeadID');
                String RelatedToId = (String) inputMap.get('RelatedToId');
                String OrgWideEmailAddress = (String) inputMap.get('OrgWideEmailAddress');
                CallApexFromOmniScript_UTL.sendEmailFuture(EmailTemplateName,ToAddresses,TreatTargetObjectAsRecipient,ContactOrLeadID,RelatedToId,OrgWideEmailAddress);
            } 
            
            else if(methodName == CREATEUPDATEQUOTE){
                String inputKey = (String) inputMap.get('inputKey');
                String pricebook = (String) inputMap.get('pricebook');
                CallApexFromOmniScript_UTL.createUpdateQuote(inputKey,pricebook);
            } 
            else if(methodName == UPDATEPOLICY){
                String effectiveDate = (String) inputMap.get('effectiveDate');
                String inputKey = (String) inputMap.get('inputKey');
                Boolean generatePolicyNumber = (Boolean) inputMap.get('generatePolicyNumber');
                CallApexFromOmniScript_UTL.createUpdatePolicy(effectiveDate,inputKey,generatePolicyNumber);
            }
            
        } catch (Exception e) {
            outputMap.put('response', e.getMessage());
        }
        return null;
    }
    @future(callout=true)
    public static void sendEmailFuture(String EmailTemplateName,String ToAddresses,Boolean TreatTargetObjectAsRecipient,String ContactOrLeadID,String RelatedToId,String OrgWideEmailAddress){
        GLSendEmail.SendEmail.InvokeSendEmail request = new GLSendEmail.SendEmail.InvokeSendEmail();
            request.EmailTemplateName = EmailTemplateName;
            request.ToAddresses = ToAddresses;
            request.TreatTargetObjectAsRecipient = (Boolean)TreatTargetObjectAsRecipient;
            request.ContactOrLeadID = ContactOrLeadID;
            request.RelatedToId = RelatedToId;
            request.OrgWideEmailAddress = OrgWideEmailAddress;
            GLSendEmail.SendEmail.SendEmail(new List<GLSendEmail.SendEmail.InvokeSendEmail>{request});
    }
    @future(callout=true)
    public static void createUpdateQuote(String inputKey,String pricebook){
        Map<String, Object> outputObj = new Map<String, Object> (); 
        Map<String, Object> inputObj = new Map<String, Object> ();
        Map<String, Object> ipOptions = new Map<String, Object> ();

        inputObj.put('inputKey',inputKey);
        inputObj.put('pricebook',pricebook);

        string procedureName = 'quote_quoteService';

        outputObj = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName,inputObj,ipOptions);
    }

    @future(callout=true)
    public static void createUpdatePolicy(String effectiveDate,String inputKey,Boolean generatePolicyNumber){
        Map<String, Object> outputObj = new Map<String, Object> (); 
        Map<String, Object> inputObj = new Map<String, Object> ();
        Map<String, Object> ipOptions = new Map<String, Object> ();

        inputObj.put('effectiveDate',effectiveDate);
        inputObj.put('inputKey',inputKey);
        inputObj.put('generatePolicyNumber',generatePolicyNumber);

        string procedureName = 'VSP_IP02createUpdatePolicy';

        outputObj = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName,inputObj,ipOptions);
    }

}