/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 02-14-2022
 * @last modified by  : David Mignane Diop
**/
global class PaymentSinistreJob_SVE {
    @InvocableMethod(label='paymentSinistre' description='payement sinistre ' category='vlocity_ins__ClaimLineItem__c')
    public static void createClaim(list<Id> claimLineItemIds){
        System.debug('claimLineItemIds: '+claimLineItemIds);
        payerSinistre(claimLineItemIds);
    }
    public Static void payerSinistre (list<Id> claimLineItemIds){
        System.debug('tester si le flow a été lancer'+claimLineItemIds);
        List<Id> claimLineIds =new List<Id>();
        List<vlocity_ins__ClaimLineItem__c> claimLineItemList =  new List<vlocity_ins__ClaimLineItem__c>();
        String procedureName = 'cliamLine_sinistre';
        for(Integer i=0;i<claimLineItemIds.size();i++){
      		if(i<3){
                System.debug('if:');
                //ClaimPaymentFuture.CallClaimPayment(claimLineItemIds[i],procedureName);
      		}else{
                System.debug('else:');
                //claimLineIds.add(claimLineItemIds[i]);
      		} 
  		} 
        if(!claimLineIds.isEmpty()){
            System.debug('Dev of David:');
      	    //ID jobID = System.enqueueJob(new ClaimLineQueuebleJob(claimLineIds));
  		}
    }
}