/**
 * @author Amir BEN SALEM (Salesforce)
 * @date 30.09.2021
 * @description classe d'instantiation  de l'objet Commandeslistemobiles_WSI
 */
public with sharing class Commandeslistemobiles_WSI extends APIRequest_WSI{
    public String id_facture;
    public String id_magasin;
    public String nom_client;

    public Commandeslistemobiles_WSI (String id_facture ,string id_magasin,String nom_client){
        this.id_facture  = id_facture;
        this.id_magasin  = id_magasin;
        this.nom_client  = nom_client;
    }
    
    public override Map<String, String> getParameters() {
        Map<String, String> parameters = new Map<String, String>();
       // parameters.put('id_facture', id_facture);
        parameters.put('id_magasin', id_magasin);
        parameters.put('nom_client', nom_client);
        return parameters;
    }

    public override String getFullUrl (  Map<String,String> parameters , String endPoint){
        endPoint=endPoint+'/'+EncodingUtil.urlEncode(id_facture,'UTF-8').replaceAll('\\+','%20');
        Pagereference pgEndPoint = new Pagereference(endPoint);
        if(parameters != null){
            for(String key : parameters.keySet()){
                String value = parameters.get(key);
                pgEndPoint.getParameters().put(key, value);
            }
                 return pgEndPoint.getUrl();
        }
        else {
                return null;
        }

    }

    
}