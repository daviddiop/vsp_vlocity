/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 01-11-2022
 * @last modified by  : David Mignane Diop
**/
public with sharing class EnvoyerSMS_WSI extends APIRequest_WSI{
    public String message;
    public  String expediteur;
    public String [] destinataires;
    public EnvoyerSMS_WSI(String message,String[] destinataire ,String expediteur) {
        this.message = message;
        this.destinataires = destinataire;
        this.expediteur = expediteur;
    }
}