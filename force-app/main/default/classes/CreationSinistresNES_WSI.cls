/**
 * @description       : 
 * @author            : David  Diop
 * @group             : 
 * @last modified on  : 12-03-2021
 * @last modified by  : David Mignane Diop
**/
public with sharing class CreationSinistresNES_WSI extends APIRequest_WSI {
    public String id_sinistre_verspieren_sf ;
    public String description ;
    public String date_survenance ;
    public String date_declaration ;
    public ContratDTO contrat ;
    public produitSinistreDTO produit_sinistre ;

    /*public CreationSinistresNES_WSI(vlocity_ins__InsuranceClaim__c claimRecord) {
        this.id_sinistre_verspieren_sf = claimRecord.Id;
        this.description = claimRecord.vlocity_ins__Description__c;
        CreationSinistresNES_WSI.adresseDTO adresse= new CreationSinistresNES_WSI.adresseDTO();
        CreationSinistresNES_WSI.offreDTO offre = new CreationSinistresNES_WSI.offreDTO();
        CreationSinistresNES_WSI.clientDTO client= new CreationSinistresNES_WSI.clientDTO();
        CreationSinistresNES_WSI.produitSinistreDTO produitSinistre = new CreationSinistresNES_WSI.produitSinistreDTO();
        CreationSinistresNES_WSI.marqueDTO  marque= new CreationSinistresNES_WSI.marqueDTO();
        CreationSinistresNES_WSI.categorieDTO categorie= new CreationSinistresNES_WSI.categorieDTO();
        CreationSinistresNES_WSI.familleDTO famille = new CreationSinistresNES_WSI.familleDTO();

            produitSinistre.marque = marque;
            produitSinistre.categorie = categorie;
            produitSinistre.famille = famille;
        CreationSinistresNES_WSI.ContratDTO contrat = new CreationSinistresNES_WSI.ContratDTO();
            contrat.offre = offre;
            contrat.client = client;
    }*/
    public CreationSinistresNES_WSI() {
        
    }
    public static String validatefield(vlocity_ins__InsuranceClaim__c claimRecord){
        String message = '';
        if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.FirstName == null ){
            message= 'le prenom est obligatoire pour cette requete'; 
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.LastName == null ){
            message= 'le nom est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.PersonMobilePhone == null ){
            message= 'le telephone est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.PersonEmail == null ){
            message= 'le mail est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingStreet == null ){
            message= 'la ville est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingCity == null ){
            message= 'le pays est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingPostalCode == null ){
            message= 'le code postal  est obligatoire pour cette requete';
            return message;
        } 
        /*else if(claimRecord.ClaimDeviceCode__c == null ){
            message= 'le produit code est obligatoire pour cette requete';
            return message;
        }*/
        else if(claimRecord.ClaimDeviceModel__c == null ){
            message= 'le produit name est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.ClaimDeviceFamily__c == null ){
            message= 'le code famille  est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.ProductCategory__c == null ){
            message= 'le categorie code   est obligatoire pour cette requete';
            return message;
        }
        else if(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Tech_ProductUnivers__c == null ){
            message= 'le nom de la categodie  est obligatoire pour cette requete';
            return message;
        }
        else if (claimRecord.vlocity_ins__Description__c == null ){
            message= 'la description  est obligatoire pour cette requete';
            return message;
        }
        else if (claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.vlocity_ins__EffectiveDate__c == null ){
            message= 'la date effet  est obligatoire pour cette requete';
            return message;
        } else if(claimRecord.Tech_AcceptedRemainingAmount__c == null){
            message= 'le solde_indemnisation  est obligatoire pour cette requete';
            return message;
        } else if(claimRecord.WarrantyType__c == null){
            message= 'le garantie code du sinistre    est obligatoire pour cette requete';
            return message;
        }
        return message;
    }
    public static Map<String,String> getclaimDeviceTypeValue(){
        Map<String,String > pickListValuesList = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__InsuranceClaim__c.ClaimDeviceType__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.put(pickListVal.getLabel(),pickListVal.getValue());
        }
        return pickListValuesList;
    }
    public static Map<String,String> getproductCategoryValue(){
        Map<String,String > pickListValuesList = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Asset.ProductCategory__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.put(pickListVal.getValue(),pickListVal.getLabel());
        }
        return pickListValuesList;
    }

    public CreationSinistresNES_WSI createSinistre(vlocity_ins__InsuranceClaim__c claimRecord){
        Claim_QR claimQuery =  new Claim_QR();
        vlocity_ins__ClaimCoverage__c  claimCoverage =  claimQuery.getClaimCoverage(claimRecord.Id);
        /*[
            SELECT Id,vlocity_ins__AssetCoverageId__r.vlocity_ins__Product2Id__r.CarrierContract__r.PolicyConvetionNumber__c
             from vlocity_ins__ClaimCoverage__c 
             WHERE vlocity_ins__ClaimId__c   =: claimRecord.Id 
             LIMIT 1
        ];*/

        CreationSinistresNES_WSI sinistre = new CreationSinistresNES_WSI();
        

        CreationSinistresNES_WSI.adresseDTO adresse= new CreationSinistresNES_WSI.adresseDTO();
        CreationSinistresNES_WSI.offreDTO offre = new CreationSinistresNES_WSI.offreDTO();
        CreationSinistresNES_WSI.clientDTO client = new CreationSinistresNES_WSI.clientDTO();
        CreationSinistresNES_WSI.produitSinistreDTO produitSinistre = new CreationSinistresNES_WSI.produitSinistreDTO();
        CreationSinistresNES_WSI.marqueDTO  marque= new CreationSinistresNES_WSI.marqueDTO();
        CreationSinistresNES_WSI.categorieDTO categorie= new CreationSinistresNES_WSI.categorieDTO();
        CreationSinistresNES_WSI.ContratDTO contrat = new CreationSinistresNES_WSI.ContratDTO();
        //famille
        CreationSinistresNES_WSI.familleDTO famille = new CreationSinistresNES_WSI.familleDTO();
        if(claimRecord.ClaimDeviceFamily__c != null){
            if( claimRecord.ClaimDeviceFamily__c.length() >= 10){
                famille.code = claimRecord.ClaimDeviceFamily__c.SubString(0,9);
            }else {
                famille.code = claimRecord.ClaimDeviceFamily__c;
            }
        }
        //famille.nom = claimRecord.ClaimDeviceFamily__c; modif avec le produit type 
        
        Map<String,String> claimDeviceTypeList = getclaimDeviceTypeValue();
        famille.nom = claimRecord.ClaimDeviceType__c;
        //categorie
        Map<String,String> productCategoryValue = getproductCategoryValue();
        System.debug('categorie.code'+productCategoryValue.get(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.ProductCategory__c));
        //categorie.code = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.ProductCategory__c ;
        categorie.code = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Tech_ProductUnivers__c;
        categorie.nom = productCategoryValue.get(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.ProductCategory__c);
        //marque
        marque.code = claimRecord.ClaimDeviceBrand__c;
        if(claimRecord.ClaimDeviceBrand__c == 'Autre'){
            marque.nom = claimRecord.OtherClaimDeviceBrand__c;
        }else {
            marque.nom = claimRecord.ClaimDeviceBrand__c;
        }
        //produitSinistre
        String productCodeMobile = 'ED_CLAIM_ELECTROSUR_MOBILE';
        String productCode = 'ED_CLAIM_ELECTROSUR';
        if(claimRecord.vlocity_ins__SpecProduct2Id__r.ProductCode == productCodeMobile ){
            if(claimRecord.ClaimDeviceCode__c != null){
                if( claimRecord.ClaimDeviceCode__c.length() > 20){
                    produitSinistre.code = claimRecord.ClaimDeviceCode__c.SubString(0,20);
                }else {
                    produitSinistre.code = claimRecord.ClaimDeviceCode__c;
                }
            } else {
                produitSinistre.code = '';
            }
        } else {
            produitSinistre.code = 'GENERIC';
        }
        if(claimRecord.ClaimDeviceCode__c != null){
            if( claimRecord.ClaimDeviceCode__c.length() > 20){
            produitSinistre.code = claimRecord.ClaimDeviceCode__c.SubString(0,19);
            }else {
            produitSinistre.code = claimRecord.ClaimDeviceCode__c;
            }
            }
        
        if(claimRecord.ClaimDeviceModel__c != null){
            if( claimRecord.ClaimDeviceModel__c.length() > 100){
                produitSinistre.nom = claimRecord.ClaimDeviceModel__c.SubString(0,99);
            }else {
                produitSinistre.nom = claimRecord.ClaimDeviceModel__c;
            }
            
        }
        produitSinistre.date_achat = string.valueof(claimRecord.ClaimDeviceInvoiceDate__c);
        produitSinistre.prix_achat_net_ttc = claimRecord.ClaimDeviceAmount__c;//ClaimDeviceAmount__c
        produitSinistre.numero_serie = '0000';
        if(claimRecord.ClaimImei__c != null){
            produitSinistre.imei = string.valueof(claimRecord.ClaimImei__c);
        } else {
            produitSinistre.imei = '0000';
        }
        if(claimRecord.ClaimDeviceInvoiceNumber__c.length() > 20){
        	produitSinistre.numero_facture  = claimRecord.ClaimDeviceInvoiceNumber__c.SubString(0,20);
        }
        else{
            produitSinistre.numero_facture  = claimRecord.ClaimDeviceInvoiceNumber__c;
        }
        produitSinistre.marque = marque;
        produitSinistre.categorie = categorie;
        produitSinistre.famille = famille ;

        //Split D'adresse sur 4 ligne de 50 caractéres  
        String adresseClient = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingStreet;
        
        if(adresseClient.length() <= 50){
        	adresse.ligne_1 = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingStreet;
        	adresse.ligne_2 = ''; //claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingStreet;//à revoir
        	adresse.ligne_3 = ''; //claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingStreet;//à revoir
        	adresse.ligne_4 = '';//claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingStreet;//à revoir    
        }
        else{
            String ligne = '';
            List<String> lignesAdresse = new List<String>();
            List<String> adresseSplittedbySpace = adresseClient.split(' ');
            
			for (String str : adresseSplittedbySpace){
    			system.debug(str);
                String ligneInterm;
                if(ligne != '') {ligneInterm = ligne + ' ' + str;} 
                else {ligneInterm = str;}
                
                if(ligneInterm.length()<= 50) {
                    ligne = ligneInterm;
                }
                else {
                    lignesAdresse.add(ligne);
                    ligne=str;
                    
                }
			}
            lignesAdresse.add(ligne);
            system.debug(lignesAdresse);
            if(lignesAdresse.size()>=1){adresse.ligne_1 = lignesAdresse[0];} else {adresse.ligne_1 = '';}
            if(lignesAdresse.size()>=2){adresse.ligne_2 = lignesAdresse[1];} else {adresse.ligne_2 = '';}
            if(lignesAdresse.size()>=3){adresse.ligne_3 = lignesAdresse[2];} else {adresse.ligne_3 = '';}
            if(lignesAdresse.size()>=4){adresse.ligne_4 = lignesAdresse[3];} else {adresse.ligne_4 = '';}
        }
        
        
        
        adresse.ville = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingCity;
        adresse.code_postal = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.BillingPostalCode;
        adresse.pays = 'FR';
        //client
        client.prenom = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.FirstName ;
        client.nom = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.LastName;
        client.telephone = '+'+claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.PersonMobilePhone;
        client.email = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Account.PersonEmail;
        client.adresse = adresse;
        //offre
        offre.code = claimRecord.Name;
        offre.nom = claimRecord.WarrantyType__c ;
        //offre.nom = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.ProductName__c ;
        //contrat
        if(claimRecord.Tech_PolicyCodeNumber__c != null){
            contrat.numero_contrat_collectif = claimRecord.Tech_PolicyCodeNumber__c;
        } else {
            contrat.numero_contrat_collectif = '';
        }
        contrat.solde_indemnisation = claimRecord.Tech_AcceptedRemainingAmount__c ; //ok
        contrat.id_contrat_verspieren_sf = claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.Ext_OGIContractID__c;
        contrat.date_effet = string.valueof(claimRecord.vlocity_ins__PrimaryPolicyAssetId__r.vlocity_ins__EffectiveDate__c) ;
        contrat.offre = offre ;
        contrat.client = client ;

        //sinistre
        sinistre.id_sinistre_verspieren_sf = claimRecord.Id;
        sinistre.description = claimRecord.vlocity_ins__Description__c;
        sinistre.date_survenance = string.valueof(claimRecord.vlocity_ins__LossDate__c);
        sinistre.date_declaration = string.valueof(claimRecord.vlocity_ins__ReportedDate__c);
        sinistre.contrat = contrat;
        sinistre.produit_sinistre = produitSinistre;
        System.debug('contrat'+contrat);
        System.debug('offre'+offre);
        System.debug('client'+client);
        System.debug('adresse'+adresse);
        System.debug('produitSinistre'+produitSinistre);
        System.debug('marque'+marque);
        System.debug('categorie'+categorie);
        System.debug('famille'+famille);
        return sinistre;
    }
   
    public  class ContratDTO{
        public String numero_contrat_collectif;
        public Decimal solde_indemnisation;
        public String id_contrat_verspieren_sf;
        public String date_effet;
        public  offreDTO offre;
        public  clientDTO client;
    }

    public class offreDTO{
        public String code;
        public String nom;
    }
    
    public class clientDTO{
        public  adresseDTO adresse;
        public String prenom;
        public String nom;
        public String telephone;
        public String email;
    }

    public class adresseDTO{
        public String ligne_1;
        public String ligne_2;
        public String ligne_3;
        public String ligne_4;
        public String ville;
        public String code_postal;
        public String pays;
    }

    public class produitSinistreDTO {
        public String code;
        public String nom;
        public String date_achat;
        public Decimal prix_achat_net_ttc;
        public String numero_serie;
        public String imei;
        public  String numero_facture;
        public  marqueDTO marque;
        public  categorieDTO categorie;
        public  familleDTO famille;
    }
    
    public class marqueDTO{
        public String code;
        public String nom;
    }

    public class categorieDTO{
        public String code;
        public String nom;
    }

    public class familleDTO{
        public String code;
        public String nom ;
    }
}