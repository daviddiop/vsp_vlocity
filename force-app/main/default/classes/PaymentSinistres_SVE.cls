/**
 * @description       : 
 * @author            : David Mignane Diop
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : David Mignane Diop
**/
global with sharing class PaymentSinistres_SVE { 
    /**
     * @description
     * @param  : list<Id> claimLineItemIds
     * @return 
     */
    @InvocableMethod(label='paymentSinistre' description='payement sinistre ' category='vlocity_ins__ClaimLineItem__c')
    public static void createClaim(list<Id> claimLineItemIds){
        payerSinistre(claimLineItemIds);
    }
    //@future(callout=true)
    public Static void payerSinistre (list<Id> claimLineItemIds){
        
        System.debug('tester si le flow a été lancer: '+claimLineItemIds);
        List<vlocity_ins__ClaimLineItem__c> claimLineItemList =  new List<vlocity_ins__ClaimLineItem__c>();
      /*  for(Id claimLineItemId:claimLineItemIds){
            vlocity_ins__ClaimLineItem__c claimItem = new vlocity_ins__ClaimLineItem__c();
            claimItem.Id = claimLineItemId;
            //claimItem.vlocity_ins__Status__c = 'Paid';
            claimLineItemList.add(claimItem);
        }
        Upsert claimLineItemList;*/
        String procedureName = 'cliamLine_sinistre';
        if((!claimLineItemIds.isEmpty()) && (ClaimLineItemIds.size() > 5)) 
        {
             ClaimLineItemBatch objbatch = new ClaimLineItemBatch(claimLineItemIds);  
             Database.executeBatch(objbatch, 5);
        }
        else if((!claimLineItemIds.isEmpty()) && (ClaimLineItemIds.size() <= 5))
        {
            ID jobID = System.enqueueJob(new ClaimLineQueuebleJob(claimLineItemIds));
        }
        /*for(Id claimLineItemId:claimLineItemIds){
         	ID jobID = System.enqueueJob(new ClaimLineQueuebleJob(claimLineItemIds));
            CallClaimPayment(claimLineItemId,procedureName);
       
    		}*/
        

    }
    
    /*@future
    public Static void CallClaimPayment (Id claimLineItemId , String ProcedureName){
        Map<String, Object> outputObj = new Map<String, Object> (); 
        Map<String, Object> inputObj = new Map<String, Object> ();
        Map<String, Object> ipOptions = new Map<String, Object> ();
        
        inputObj.put('claimLineItemId',claimLineItemId);
        try 
          {
            
            outputObj = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName,inputObj,ipOptions);
            System.debug('test output reponse apres l appel du service vlocity'+outputObj);
          } 
        catch (Exception e)
        {
            System.debug('++++++++ eCause '+ e.getCause());
             
        }
        
    }*/
    
}