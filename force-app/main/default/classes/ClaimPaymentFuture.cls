public class ClaimPaymentFuture {
    public Static String CallClaimPayment (Id claimLineItemId , String ProcedureName){
        Map<String, Object> outputObj = new Map<String, Object> (); 
        Map<String, Object> inputObj = new Map<String, Object> ();
        Map<String, Object> ipOptions = new Map<String, Object> ();
        
        inputObj.put('claimLineItemId',claimLineItemId);
        try 
          {
            
            outputObj = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName,inputObj,ipOptions);
            System.debug('test output reponse apres l appel du service vlocity'+outputObj); 
             return json.serialize(outputObj);
          } 
        catch (Exception e)
        {
            System.debug('++++++++ eCause '+ e.getCause());
            return 'Catch Error: '+e.getCause();
        }
        
    }
    
    public Static void CallClaimPaymentTest(List<Id> claimeLineItemIds , String ProcedureName){
        Map<String, Object> outputObj = new Map<String, Object> (); 
        Map<String, Object> inputObj = new Map<String, Object> ();
        Map<String, Object> ipOptions = new Map<String, Object> ();
        List<Map<String, Id>> mapOfIDS = new List<Map<String, Id>>();
        for(Id idVal : claimeLineItemIds)
        {
            mapOfIDS.add(new Map<String, Id>{'Id' => idVal});
        }
        inputObj.put('itemIds', mapOfIDS);
        //inputObj.put('claimLineItemId',claimeLineItemIds[0]);
        try 
          {
            System.debug('inputObj: '+inputObj);
            outputObj = (Map<String,Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName,inputObj,ipOptions);
            System.debug('test output reponse apres l appel du service vlocity'+outputObj); 
          } 
        catch (Exception e)
        {
            System.debug('++++++++ eCause '+ e.getCause()); 
        }
        
    }
}