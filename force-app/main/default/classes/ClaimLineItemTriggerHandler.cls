public without sharing class ClaimLineItemTriggerHandler implements TriggerHandler {
    private boolean triggerIsExecuting;
    private integer triggerSize;
    public ClaimLineItemTriggerHelper helper;
    public ClaimLineItemTriggerHandler(boolean triggerIsExecuting, integer triggerSize) {
        this.triggerIsExecuting = triggerIsExecuting;
        this.triggerSize = triggerSize;
        this.helper = new ClaimLineItemTriggerHelper();
    }
     public void beforeInsert(List<vlocity_ins__ClaimLineItem__c> newClaimLineItems) {
        // helper.doTask1();
        // helper.doTask2();
    }
    public void beforeUpdate(List<vlocity_ins__ClaimLineItem__c> oldClaimLineItems, List<vlocity_ins__ClaimLineItem__c> newClaimLineItems, Map<ID, SObject> oldClaimLineItemMap, Map<ID, SObject> newClaimLineItemMap) {
        List<vlocity_ins__ClaimLineItem__c> lstClaimLineItemChangedToPaied = new List<vlocity_ins__ClaimLineItem__c>();
        Map<ID, vlocity_ins__ClaimLineItem__c> oldClaimLineItemMapAfterCast = (Map<ID, vlocity_ins__ClaimLineItem__c>)oldClaimLineItemMap;
        Map<ID, vlocity_ins__ClaimLineItem__c> newClaimLineItemMapAfterCast = (Map<ID, vlocity_ins__ClaimLineItem__c>)newClaimLineItemMap;
        for (Id idClaimLineItem : newClaimLineItemMap.keySet()) { 
            if((newClaimLineItemMapAfterCast.get(idClaimLineItem).Ext_AccountingStatus__c  != oldClaimLineItemMapAfterCast.get(idClaimLineItem).Ext_AccountingStatus__c) && (newClaimLineItemMapAfterCast.get(idClaimLineItem).Ext_AccountingStatus__c == '02'))
            {
                lstClaimLineItemChangedToPaied.add(newClaimLineItemMapAfterCast.get(idClaimLineItem));
            }
         }
        if(lstClaimLineItemChangedToPaied.size() > 0)
        {
            System.debug('lstClaimLineItemChangedToPaied: '+lstClaimLineItemChangedToPaied.size());
            helper.CreateClaim(lstClaimLineItemChangedToPaied);
        }

    }
    public void beforeDelete(List<vlocity_ins__ClaimLineItem__c> oldClaimLineItems, Map<ID, SObject> oldClaimLineItemMap) {
        // helper.doTask5();
        // helper.doTask1();
    }
    public void afterInsert(List<vlocity_ins__ClaimLineItem__c> newClaimLineItems, Map<ID, SObject> newClaimLineItemMap) {
        // helper.doTask2();
        // helper.doTask3();
    }
    public void afterUpdate(List<vlocity_ins__ClaimLineItem__c> oldClaimLineItems, List<vlocity_ins__ClaimLineItem__c> newClaimLineItems, Map<ID, SObject> oldClaimLineItemMap, Map<ID, SObject> newClaimLineItemMap) {
        // helper.doTask4();
        // helper.doTask5();
    }
    public void afterDelete(List<vlocity_ins__ClaimLineItem__c> oldClaimLineItems, Map<ID, SObject> oldClaimLineItemMap) {
        // helper.doTask3();
        // helper.doTask1();
    }
    public void afterUndelete(List<vlocity_ins__ClaimLineItem__c> newClaimLineItems, Map<ID, SObject> newClaimLineItemMap) {
        // helper.doTask4();
        // helper.doTask2();
    }
}