Write-Output '##### DEPLOIEMENT VERS UNE SANDBOX #####'
echo '##### Attention vous allez deployer dans une sandbox #####'
$t = $host.ui.RawUI.ForegroundColor

echo ''

$ORG_ALIAS = Read-Host -Prompt 'Nom de la Sandbox sur laquelle vous souhaitez deployer ? (CRTL + C pour annuler)'

echo ''
Write-Output '##### Deploiement des sources Salesforce #####'
sfdx force:source:push -f -u $ORG_ALIAS

echo ''
$CONTINUE = Read-Host -Prompt 'Continuez avec le deploiement des sources Vlocity ? (Y ou y pour continuer)'
if ($CONTINUE -eq 'Y')
{
Write-Output '##### Deploiement des sources Vlocity #####'
vlocity '-sfdx.username' $ORG_ALIAS -job Vlocity\.vlocity\VlocityComponents.yaml packDeploy
vlocity '-sfdx.username' $ORG_ALIAS -job Vlocity\.vlocity\VlocityComponents.yaml packRetry
}